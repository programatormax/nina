unit PlanSwift9_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// $Rev: 98336 $
// File generated on 6/21/2022 12:58:43 AM from Type Library described below.

// ************************************************************************  //
// Type Lib: C:\Program Files (x86)\PlanSwift10\PlanSwift.exe (1)
// LIBID: {60A9C20E-DE24-420D-B22E-C43286CC9AA6}
// LCID: 0
// Helpfile: 
// HelpString: PlanSwift9 Library
// DepndLst: 
//   (1) v2.0 stdole, (C:\Windows\SysWOW64\stdole2.tlb)
// SYS_KIND: SYS_WIN32
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
{$ALIGN 4}

interface

uses Winapi.Windows, System.Classes, System.Variants, System.Win.StdVCL, Vcl.Graphics, Vcl.OleServer, Winapi.ActiveX;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  PlanSwift9MajorVersion = 1;
  PlanSwift9MinorVersion = 0;

  LIBID_PlanSwift9: TGUID = '{60A9C20E-DE24-420D-B22E-C43286CC9AA6}';

  IID_IPlanSwift: TGUID = '{DE10DABB-A994-452F-B92D-7CB740217FDA}';
  DIID_IPlanSwiftEvents: TGUID = '{E5B33107-8935-4AFC-8294-54D7AEDDBF75}';
  CLASS_PlanSwift: TGUID = '{B521BEFE-947B-4FDD-8EA5-6478E4CB7D1D}';
  IID_IItem: TGUID = '{037C8C66-2738-490D-953C-EA6F211A56F0}';
  CLASS_Item: TGUID = '{6BB427F4-A061-41EA-88D9-4A83D67F76A0}';
  IID_IPropertyObject: TGUID = '{325EA379-B734-4500-87BA-3C1D25F4D90A}';
  CLASS_PropertyObject: TGUID = '{EA3576A1-74E5-4A72-B9A2-F1DAAA3C5A46}';
  IID_IPoint: TGUID = '{9661175A-6FB8-4D72-A907-7551AAF03B6B}';
  IID_ISelectionList: TGUID = '{A974B0EA-8A2A-441E-96E2-6DFC79514411}';
  CLASS_SelectionList: TGUID = '{E9C7F94C-6BDC-4A79-9866-3D5EB1C363AF}';
  IID_ILine: TGUID = '{AED4DE6B-9639-4397-BFFB-B94BC73F7FC9}';

// *********************************************************************//
// Declaration of Enumerations defined in Type Library                    
// *********************************************************************//
// Constants for enum PropertyTypes
type
  PropertyTypes = TOleEnum;
const
  ptNumber = $00000000;
  ptColor = $00000001;
  ptText = $00000002;
  ptMemo = $00000003;
  ptCheckBox = $00000004;
  ptPath = $00000005;
  ptImage = $00000006;
  ptLargeImage = $00000007;
  ptType = $00000008;
  ptScript = $00000009;
  ptFile = $0000000A;
  ptLargeFile = $0000000B;
  ptFileName = $0000000C;
  ptConnectionString = $0000000D;
  ptSlider = $0000000E;
  ptDimension = $0000000F;

// Constants for enum InputTypes
type
  InputTypes = TOleEnum;
const
  inpStoreLocal = $00000000;
  inpStoreParent = $00000001;

// Constants for enum ListTypes
type
  ListTypes = TOleEnum;
const
  ltSimpleList = $00000000;
  ltList = $00000001;
  ltTreeList = $00000002;
  ltExecutePlugin = $00000003;
  ltAutoList = $00000004;

// Constants for enum InheritActions
type
  InheritActions = TOleEnum;
const
  iaNormal = $00000000;
  iaIgnore = $00000001;
  iaInheritFormula = $00000002;
  iaInheritResult = $00000003;
  iaFlatten = $00000004;

// Constants for enum ScriptTypes
type
  ScriptTypes = TOleEnum;
const
  stEvent = $00000000;
  stMethod = $00000001;

// Constants for enum ScriptLanguages
type
  ScriptLanguages = TOleEnum;
const
  slPascal = $00000000;
  slBasic = $00000001;
  slExecute = $00000002;

// Constants for enum PointTypes
type
  PointTypes = TOleEnum;
const
  ptNormal = $00000000;
  ptArc = $00000001;
  ptArcChild = $00000002;

type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IPlanSwift = interface;
  IPlanSwiftDisp = dispinterface;
  IPlanSwiftEvents = dispinterface;
  IItem = interface;
  IItemDisp = dispinterface;
  IPropertyObject = interface;
  IPropertyObjectDisp = dispinterface;
  IPoint = interface;
  IPointDisp = dispinterface;
  ISelectionList = interface;
  ISelectionListDisp = dispinterface;
  ILine = interface;
  ILineDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  PlanSwift = IPlanSwift;
  Item = IItem;
  PropertyObject = IPropertyObject;
  SelectionList = ISelectionList;


// *********************************************************************//
// Interface: IPlanSwift
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {DE10DABB-A994-452F-B92D-7CB740217FDA}
// *********************************************************************//
  IPlanSwift = interface(IDispatch)
    ['{DE10DABB-A994-452F-B92D-7CB740217FDA}']
    procedure About; safecall;
    function Edition: WideString; safecall;
    function IsBeta: WordBool; safecall;
    function OpenJob(const Path: WideString): WordBool; safecall;
    function GetItem(const FullPath: WideString): IItem; safecall;
    procedure CloseJob; safecall;
    function SelectedItem: IItem; safecall;
    function SelectedPage: IItem; safecall;
    procedure OpenJobEx; safecall;
    function NewSection(const ParentPath: WideString; const SectionName: WideString): IItem; safecall;
    function GetProperty(const ItemPath: WideString; const PropertyName: WideString): IPropertyObject; safecall;
    function GetPropertyFormula(const ItemPath: WideString; const PropertyName: WideString): WideString; safecall;
    function GetPropertyResult(const ItemPath: WideString; const PropertyName: WideString): OleVariant; safecall;
    function GetPropertyResultAsBoolean(const ItemPath: WideString; const PropertyName: WideString; 
                                        Default: WordBool): WordBool; safecall;
    function GetPropertyResultAsFloat(const ItemPath: WideString; const PropertyName: WideString; 
                                      Default: Double): Double; safecall;
    function GetPropertyResultAsInteger(const ItemPath: WideString; const PropertyName: WideString; 
                                        Default: Integer): Integer; safecall;
    function GetPropertyResultAsString(const ItemPath: WideString; const PropertyName: WideString; 
                                       const Default: WideString): WideString; safecall;
    procedure SetPropertyFormula(const ItemPath: WideString; const PropertyName: WideString; 
                                 const Value: WideString); safecall;
    function NewItem(const ParentPath: WideString; const ItemType: WideString; 
                     const AName: WideString): IItem; safecall;
    function DeleteItem(const ItemPath: WideString): WordBool; safecall;
    function DeleteProperty(const ItemPath: WideString; const PropertyName: WideString): WordBool; safecall;
    procedure BeginUpdate; safecall;
    procedure EndUpdate; safecall;
    procedure BeginFormulaUpdate; safecall;
    procedure EndFormulaUpdate; safecall;
    procedure NewPoint(const ItemPath: WideString; X: Double; Y: Double); safecall;
    function PointCount(const ItemPath: WideString): Integer; safecall;
    procedure SetPoint(const ItemPath: WideString; PointIndex: Integer; X: Double; Y: Double); safecall;
    function Root: IItem; safecall;
    procedure CancelTool; safecall;
    function Handle: HResult; safecall;
    procedure NewChangeGroup(const GroupName: WideString); safecall;
    procedure PostChanges; safecall;
    function IsUnlocked(const AProduct: WideString; AMajVer: Integer; AMinVer: Integer; 
                        AllowUnlock: WordBool): WordBool; safecall;
    function SelectionList: ISelectionList; safecall;
    function SelectItemDialog(const Header: WideString; const Title: WideString; 
                              const RootItemID: WideString): IItem; safecall;
    function CompareVersion(const VersionA: WideString; const VersionB: WideString): Integer; safecall;
    function NewItemEx(const ParentPath: WideString; const ItemType: WideString; 
                       const AName: WideString; EditProperties: WordBool): IItem; safecall;
    function CopyItem(const ItemPath: WideString; const NewParent: WideString; 
                      IncludeChildren: WordBool; SkipSections: WordBool): IItem; safecall;
    function NewBlankPage(const AName: WideString; AWidth: Integer; AHeight: Integer; 
                          ADPI: Integer; const AScale: WideString): IItem; safecall;
    function NewJobEx(const JobName: WideString): WordBool; safecall;
    function GetPoint(const ToolHint: WideString): IPoint; safecall;
    function GetLine(const ToolHint: WideString): ILine; safecall;
    function GetRect(const ToolHint: WideString): ILine; safecall;
    function Get_Zoom: Double; safecall;
    procedure Set_Zoom(Value: Double); safecall;
    function GetJobTotal(const PropertyName: WideString; const ItemType: WideString): Double; safecall;
    function IsJobOpen: WordBool; safecall;
    procedure SetSelected(const ItemPath: WideString; Value: WordBool); safecall;
    function SaveScreenShot(const FileName: WideString; Prompt: WordBool): WordBool; safecall;
    function CurrentViewport: ILine; safecall;
    function CurrentVersion: WideString; safecall;
    function DrawOneWayLayout(const AItem: WideString; const SpanLine: ILine; const RunLine: ILine; 
                              bIncludeFirst: WordBool; bIncludeLast: WordBool; nSpacing: Double; 
                              const AArea: WideString): WordBool; safecall;
    function DrawTwoWayLayout(const AItem: WideString; const SpanLine: ILine; const RunLine: ILine; 
                              bIncludeFirst: WordBool; bIncludeLast: WordBool; nSpacing: Double; 
                              const AArea: WideString): WordBool; safecall;
    function GetOneWayLayout(const AItem: WideString; const sSpanHint: WideString; 
                             const sRunHint: WideString; bIncludeFirst: WordBool; 
                             bIncludeLast: WordBool; nSpacing: Double; const AArea: WideString): WordBool; safecall;
    function GetTwoWayLayout(const AItem: WideString; const sSpanHint: WideString; 
                             const sRunHint: WideString; bIncludeFirst: WordBool; 
                             bIncludeLast: WordBool; nSpacing: Double; const AArea: WideString): WordBool; safecall;
    function RefreshStorages(const Storage: WideString): WordBool; safecall;
    function SaveScreenShotEx(const FileName: WideString; Prompt: WordBool; MaxPixelDim: Integer): WordBool; safecall;
    procedure RaiseEvent(const Msg: WideString); safecall;
    function SaveImageArray(const TargetFolder: WideString; const FilePrefix: WideString; 
                            Columns: Integer; Rows: Integer; out FinishedWidth: Integer; 
                            out FinishedHeight: Integer): WordBool; safecall;
    function ExportReport(const sGUID: WideString; const sFilespec: WideString): WordBool; safecall;
    function SendParametricXMLStruct(const XML: WideString; out sResult: WideString): WordBool; safecall;
    function AttachFile(const FileName: WideString): IItem; safecall;
    procedure MakePageActive(const GUID: WideString); safecall;
    function SelectedReport: IItem; safecall;
    function GetClassName(const ItemPath: WideString): WideString; safecall;
    function IsLoaded: WordBool; safecall;
    procedure SetActiveTab(const TabName: WideString); safecall;
    property Zoom: Double read Get_Zoom write Set_Zoom;
  end;

// *********************************************************************//
// DispIntf:  IPlanSwiftDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {DE10DABB-A994-452F-B92D-7CB740217FDA}
// *********************************************************************//
  IPlanSwiftDisp = dispinterface
    ['{DE10DABB-A994-452F-B92D-7CB740217FDA}']
    procedure About; dispid 201;
    function Edition: WideString; dispid 202;
    function IsBeta: WordBool; dispid 203;
    function OpenJob(const Path: WideString): WordBool; dispid 204;
    function GetItem(const FullPath: WideString): IItem; dispid 205;
    procedure CloseJob; dispid 206;
    function SelectedItem: IItem; dispid 207;
    function SelectedPage: IItem; dispid 208;
    procedure OpenJobEx; dispid 209;
    function NewSection(const ParentPath: WideString; const SectionName: WideString): IItem; dispid 210;
    function GetProperty(const ItemPath: WideString; const PropertyName: WideString): IPropertyObject; dispid 211;
    function GetPropertyFormula(const ItemPath: WideString; const PropertyName: WideString): WideString; dispid 212;
    function GetPropertyResult(const ItemPath: WideString; const PropertyName: WideString): OleVariant; dispid 213;
    function GetPropertyResultAsBoolean(const ItemPath: WideString; const PropertyName: WideString; 
                                        Default: WordBool): WordBool; dispid 214;
    function GetPropertyResultAsFloat(const ItemPath: WideString; const PropertyName: WideString; 
                                      Default: Double): Double; dispid 215;
    function GetPropertyResultAsInteger(const ItemPath: WideString; const PropertyName: WideString; 
                                        Default: Integer): Integer; dispid 216;
    function GetPropertyResultAsString(const ItemPath: WideString; const PropertyName: WideString; 
                                       const Default: WideString): WideString; dispid 217;
    procedure SetPropertyFormula(const ItemPath: WideString; const PropertyName: WideString; 
                                 const Value: WideString); dispid 218;
    function NewItem(const ParentPath: WideString; const ItemType: WideString; 
                     const AName: WideString): IItem; dispid 219;
    function DeleteItem(const ItemPath: WideString): WordBool; dispid 220;
    function DeleteProperty(const ItemPath: WideString; const PropertyName: WideString): WordBool; dispid 221;
    procedure BeginUpdate; dispid 222;
    procedure EndUpdate; dispid 223;
    procedure BeginFormulaUpdate; dispid 224;
    procedure EndFormulaUpdate; dispid 225;
    procedure NewPoint(const ItemPath: WideString; X: Double; Y: Double); dispid 226;
    function PointCount(const ItemPath: WideString): Integer; dispid 227;
    procedure SetPoint(const ItemPath: WideString; PointIndex: Integer; X: Double; Y: Double); dispid 228;
    function Root: IItem; dispid 229;
    procedure CancelTool; dispid 230;
    function Handle: HResult; dispid 231;
    procedure NewChangeGroup(const GroupName: WideString); dispid 232;
    procedure PostChanges; dispid 233;
    function IsUnlocked(const AProduct: WideString; AMajVer: Integer; AMinVer: Integer; 
                        AllowUnlock: WordBool): WordBool; dispid 234;
    function SelectionList: ISelectionList; dispid 235;
    function SelectItemDialog(const Header: WideString; const Title: WideString; 
                              const RootItemID: WideString): IItem; dispid 236;
    function CompareVersion(const VersionA: WideString; const VersionB: WideString): Integer; dispid 237;
    function NewItemEx(const ParentPath: WideString; const ItemType: WideString; 
                       const AName: WideString; EditProperties: WordBool): IItem; dispid 238;
    function CopyItem(const ItemPath: WideString; const NewParent: WideString; 
                      IncludeChildren: WordBool; SkipSections: WordBool): IItem; dispid 239;
    function NewBlankPage(const AName: WideString; AWidth: Integer; AHeight: Integer; 
                          ADPI: Integer; const AScale: WideString): IItem; dispid 240;
    function NewJobEx(const JobName: WideString): WordBool; dispid 241;
    function GetPoint(const ToolHint: WideString): IPoint; dispid 242;
    function GetLine(const ToolHint: WideString): ILine; dispid 243;
    function GetRect(const ToolHint: WideString): ILine; dispid 244;
    property Zoom: Double dispid 245;
    function GetJobTotal(const PropertyName: WideString; const ItemType: WideString): Double; dispid 246;
    function IsJobOpen: WordBool; dispid 247;
    procedure SetSelected(const ItemPath: WideString; Value: WordBool); dispid 248;
    function SaveScreenShot(const FileName: WideString; Prompt: WordBool): WordBool; dispid 249;
    function CurrentViewport: ILine; dispid 250;
    function CurrentVersion: WideString; dispid 251;
    function DrawOneWayLayout(const AItem: WideString; const SpanLine: ILine; const RunLine: ILine; 
                              bIncludeFirst: WordBool; bIncludeLast: WordBool; nSpacing: Double; 
                              const AArea: WideString): WordBool; dispid 252;
    function DrawTwoWayLayout(const AItem: WideString; const SpanLine: ILine; const RunLine: ILine; 
                              bIncludeFirst: WordBool; bIncludeLast: WordBool; nSpacing: Double; 
                              const AArea: WideString): WordBool; dispid 253;
    function GetOneWayLayout(const AItem: WideString; const sSpanHint: WideString; 
                             const sRunHint: WideString; bIncludeFirst: WordBool; 
                             bIncludeLast: WordBool; nSpacing: Double; const AArea: WideString): WordBool; dispid 254;
    function GetTwoWayLayout(const AItem: WideString; const sSpanHint: WideString; 
                             const sRunHint: WideString; bIncludeFirst: WordBool; 
                             bIncludeLast: WordBool; nSpacing: Double; const AArea: WideString): WordBool; dispid 255;
    function RefreshStorages(const Storage: WideString): WordBool; dispid 256;
    function SaveScreenShotEx(const FileName: WideString; Prompt: WordBool; MaxPixelDim: Integer): WordBool; dispid 257;
    procedure RaiseEvent(const Msg: WideString); dispid 258;
    function SaveImageArray(const TargetFolder: WideString; const FilePrefix: WideString; 
                            Columns: Integer; Rows: Integer; out FinishedWidth: Integer; 
                            out FinishedHeight: Integer): WordBool; dispid 259;
    function ExportReport(const sGUID: WideString; const sFilespec: WideString): WordBool; dispid 260;
    function SendParametricXMLStruct(const XML: WideString; out sResult: WideString): WordBool; dispid 261;
    function AttachFile(const FileName: WideString): IItem; dispid 262;
    procedure MakePageActive(const GUID: WideString); dispid 263;
    function SelectedReport: IItem; dispid 264;
    function GetClassName(const ItemPath: WideString): WideString; dispid 265;
    function IsLoaded: WordBool; dispid 266;
    procedure SetActiveTab(const TabName: WideString); dispid 267;
  end;

// *********************************************************************//
// DispIntf:  IPlanSwiftEvents
// Flags:     (4096) Dispatchable
// GUID:      {E5B33107-8935-4AFC-8294-54D7AEDDBF75}
// *********************************************************************//
  IPlanSwiftEvents = dispinterface
    ['{E5B33107-8935-4AFC-8294-54D7AEDDBF75}']
    function OnClose: HResult; dispid 201;
    function OnJobOpen: HResult; dispid 202;
    function OnJobClose: HResult; dispid 203;
    function OnItemDelete(const ItemPath: WideString): HResult; dispid 204;
    function OnItemChange(const ItemPath: WideString): HResult; dispid 205;
    function OnNewItem(const ItemPath: WideString): HResult; dispid 206;
    function OnDoneRecording(const ItemPath: WideString): HResult; dispid 207;
    function OnDoneRecordingDigitizer(const ItemPath: WideString): HResult; dispid 208;
    function OnSelectedPageChanged(const ItemPath: WideString): HResult; dispid 209;
    function OnNewJob(const ItemPath: WideString): HResult; dispid 210;
    function OnCopyItem(const ItemPath: WideString): HResult; dispid 211;
    function OnDigitizerSectionDelete(const ParentGUID: WideString): HResult; dispid 212;
    function OnNewChangeGroup(const Name: WideString): HResult; dispid 215;
    function OnPostChanges(const Name: WideString): HResult; dispid 216;
    function OnRaiseEvent(const Msg: WideString): HResult; dispid 213;
    function OnReceiveXML(const XML: WideString): HResult; dispid 214;
  end;

// *********************************************************************//
// Interface: IItem
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {037C8C66-2738-490D-953C-EA6F211A56F0}
// *********************************************************************//
  IItem = interface(IDispatch)
    ['{037C8C66-2738-490D-953C-EA6F211A56F0}']
    function FullPath: WideString; safecall;
    function ChildCount: Integer; safecall;
    function Get_ChildItem(Index: Integer): IItem; safecall;
    function Get_Name: WideString; safecall;
    procedure Set_Name(const Value: WideString); safecall;
    function PropertyCount: Integer; safecall;
    function Get_PropertyItem(Index: Integer): IPropertyObject; safecall;
    function Get_ItemType: WideString; safecall;
    procedure Set_ItemType(const Value: WideString); safecall;
    function NewSection(const AName: WideString): IItem; safecall;
    function Edit(ShowAdvanced: WordBool): WordBool; safecall;
    function CanRecord: WordBool; safecall;
    function DoRecord: WordBool; safecall;
    procedure Delete; safecall;
    function ParentItem: IItem; safecall;
    function NewItem(const ItemType: WideString; const AName: WideString): IItem; safecall;
    procedure NewPoint(X: Double; Y: Double); safecall;
    function PointCount: Integer; safecall;
    procedure SetPoint(PointIndex: Integer; X: Double; Y: Double); safecall;
    function NewProperty(const PropertyName: WideString; const AFormula: WideString; 
                         PropertyType: PropertyTypes): IPropertyObject; safecall;
    function GetProperty(const PropertyName: WideString): IPropertyObject; safecall;
    function GetPropertyFormula(const PropertyName: WideString): WideString; safecall;
    function GetPropertyResult(const PropertyName: WideString): OleVariant; safecall;
    function GetPropertyResultAsBoolean(const PropertyName: WideString; Default: WordBool): WordBool; safecall;
    function GetPropertyResultAsFloat(const PropertyName: WideString; Default: Double): Double; safecall;
    function GetPropertyResultAsInteger(const PropertyName: WideString; Default: Integer): Integer; safecall;
    function GetPropertyResultAsString(const PropertyName: WideString; const Default: WideString): WideString; safecall;
    function GetItem(const ItemPath: WideString): IItem; safecall;
    procedure SetPropertyFormula(const PropertyName: WideString; const Value: WideString); safecall;
    function GUID: WideString; safecall;
    function GetItemByGUID(const aGUID: WideString): IItem; safecall;
    function GetPoint(PointIndex: Integer): IPoint; safecall;
    procedure DeleteProperty(const PropertyName: WideString); safecall;
    procedure ZoomToSection(SectionIndex: Integer; Margin: Integer); safecall;
    procedure ZoomTo(Margin: Integer); safecall;
    function NewItemEx(const ItemType: WideString; const AName: WideString; EditProperties: WordBool): IItem; safecall;
    function HasChildren: WordBool; safecall;
    procedure LoadChildren; safecall;
    function SetImagePropertyFromFile(const APropertyName: WideString; const AFile: WideString): WordBool; safecall;
    function Get_Visible: WordBool; safecall;
    procedure Set_Visible(Value: WordBool); safecall;
    function Get_Selected: WordBool; safecall;
    procedure Set_Selected(Value: WordBool); safecall;
    function DoRecordEx: WordBool; safecall;
    function GetClassName: WideString; safecall;
    property ChildItem[Index: Integer]: IItem read Get_ChildItem; default;
    property Name: WideString read Get_Name write Set_Name;
    property PropertyItem[Index: Integer]: IPropertyObject read Get_PropertyItem;
    property ItemType: WideString read Get_ItemType write Set_ItemType;
    property Visible: WordBool read Get_Visible write Set_Visible;
    property Selected: WordBool read Get_Selected write Set_Selected;
  end;

// *********************************************************************//
// DispIntf:  IItemDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {037C8C66-2738-490D-953C-EA6F211A56F0}
// *********************************************************************//
  IItemDisp = dispinterface
    ['{037C8C66-2738-490D-953C-EA6F211A56F0}']
    function FullPath: WideString; dispid 201;
    function ChildCount: Integer; dispid 202;
    property ChildItem[Index: Integer]: IItem readonly dispid 0; default;
    property Name: WideString dispid 203;
    function PropertyCount: Integer; dispid 204;
    property PropertyItem[Index: Integer]: IPropertyObject readonly dispid 205;
    property ItemType: WideString dispid 206;
    function NewSection(const AName: WideString): IItem; dispid 207;
    function Edit(ShowAdvanced: WordBool): WordBool; dispid 208;
    function CanRecord: WordBool; dispid 209;
    function DoRecord: WordBool; dispid 210;
    procedure Delete; dispid 211;
    function ParentItem: IItem; dispid 212;
    function NewItem(const ItemType: WideString; const AName: WideString): IItem; dispid 213;
    procedure NewPoint(X: Double; Y: Double); dispid 214;
    function PointCount: Integer; dispid 215;
    procedure SetPoint(PointIndex: Integer; X: Double; Y: Double); dispid 216;
    function NewProperty(const PropertyName: WideString; const AFormula: WideString; 
                         PropertyType: PropertyTypes): IPropertyObject; dispid 217;
    function GetProperty(const PropertyName: WideString): IPropertyObject; dispid 218;
    function GetPropertyFormula(const PropertyName: WideString): WideString; dispid 219;
    function GetPropertyResult(const PropertyName: WideString): OleVariant; dispid 220;
    function GetPropertyResultAsBoolean(const PropertyName: WideString; Default: WordBool): WordBool; dispid 221;
    function GetPropertyResultAsFloat(const PropertyName: WideString; Default: Double): Double; dispid 222;
    function GetPropertyResultAsInteger(const PropertyName: WideString; Default: Integer): Integer; dispid 223;
    function GetPropertyResultAsString(const PropertyName: WideString; const Default: WideString): WideString; dispid 224;
    function GetItem(const ItemPath: WideString): IItem; dispid 225;
    procedure SetPropertyFormula(const PropertyName: WideString; const Value: WideString); dispid 226;
    function GUID: WideString; dispid 227;
    function GetItemByGUID(const aGUID: WideString): IItem; dispid 228;
    function GetPoint(PointIndex: Integer): IPoint; dispid 229;
    procedure DeleteProperty(const PropertyName: WideString); dispid 230;
    procedure ZoomToSection(SectionIndex: Integer; Margin: Integer); dispid 231;
    procedure ZoomTo(Margin: Integer); dispid 232;
    function NewItemEx(const ItemType: WideString; const AName: WideString; EditProperties: WordBool): IItem; dispid 233;
    function HasChildren: WordBool; dispid 234;
    procedure LoadChildren; dispid 235;
    function SetImagePropertyFromFile(const APropertyName: WideString; const AFile: WideString): WordBool; dispid 236;
    property Visible: WordBool dispid 237;
    property Selected: WordBool dispid 238;
    function DoRecordEx: WordBool; dispid 239;
    function GetClassName: WideString; dispid 240;
  end;

// *********************************************************************//
// Interface: IPropertyObject
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {325EA379-B734-4500-87BA-3C1D25F4D90A}
// *********************************************************************//
  IPropertyObject = interface(IDispatch)
    ['{325EA379-B734-4500-87BA-3C1D25F4D90A}']
    function Get_Name: WideString; safecall;
    procedure Set_Name(const Value: WideString); safecall;
    function Get_Formula: WideString; safecall;
    procedure Set_Formula(const Value: WideString); safecall;
    function Get_Units: WideString; safecall;
    procedure Set_Units(const Value: WideString); safecall;
    function Get_InputUnits: WideString; safecall;
    procedure Set_InputUnits(const Value: WideString); safecall;
    function Get_Group: WideString; safecall;
    procedure Set_Group(const Value: WideString); safecall;
    function PropertyType: WideString; safecall;
    function ResultAsString: WideString; safecall;
    function ResultAsFloat: Double; safecall;
    function ResultAsVariant: OleVariant; safecall;
    function ResultAsInteger: Integer; safecall;
    function SystemHidden: WordBool; safecall;
    function SystemLocked: WordBool; safecall;
    function Get_IsInput: WordBool; safecall;
    procedure Set_IsInput(Value: WordBool); safecall;
    function Get_InputType: InputTypes; safecall;
    procedure Set_InputType(Value: InputTypes); safecall;
    function Get_UserLocked: WordBool; safecall;
    procedure Set_UserLocked(Value: WordBool); safecall;
    function Get_UserHidden: WordBool; safecall;
    procedure Set_UserHidden(Value: WordBool); safecall;
    function Get_Adjust: WideString; safecall;
    procedure Set_Adjust(const Value: WideString); safecall;
    function Get_DecimalPlaces: Integer; safecall;
    procedure Set_DecimalPlaces(Value: Integer); safecall;
    function Get_InputCondition: WideString; safecall;
    procedure Set_InputCondition(const Value: WideString); safecall;
    function Get_Expression: WordBool; safecall;
    procedure Set_Expression(Value: WordBool); safecall;
    function Get_ListType: ListTypes; safecall;
    procedure Set_ListType(Value: ListTypes); safecall;
    function Get_SimpleList: WideString; safecall;
    procedure Set_SimpleList(const Value: WideString); safecall;
    function Get_List: WideString; safecall;
    procedure Set_List(const Value: WideString); safecall;
    function Get_ListResultColumn: WideString; safecall;
    procedure Set_ListResultColumn(const Value: WideString); safecall;
    function Get_TreeList: WideString; safecall;
    procedure Set_TreeList(const Value: WideString); safecall;
    function Get_ListFromProperty: WordBool; safecall;
    procedure Set_ListFromProperty(Value: WordBool); safecall;
    function Get_ListShowSearch: WordBool; safecall;
    procedure Set_ListShowSearch(Value: WordBool); safecall;
    function MeetsInputCondition: WordBool; safecall;
    function Get_ListColumnAutoWidth: WordBool; safecall;
    procedure Set_ListColumnAutoWidth(Value: WordBool); safecall;
    function Get_ListVisibleColumnsInDropdown: WideString; safecall;
    procedure Set_ListVisibleColumnsInDropdown(const Value: WideString); safecall;
    function Get_ListPropertiesToSet: WideString; safecall;
    procedure Set_ListPropertiesToSet(const Value: WideString); safecall;
    function Get_ListShowOnlyTypes: WideString; safecall;
    procedure Set_ListShowOnlyTypes(const Value: WideString); safecall;
    function Get_ListReturnFullPath: WordBool; safecall;
    procedure Set_ListReturnFullPath(Value: WordBool); safecall;
    function Get_ListShow1Level: WordBool; safecall;
    procedure Set_ListShow1Level(Value: WordBool); safecall;
    function Get_PluginToExecute: WideString; safecall;
    procedure Set_PluginToExecute(const Value: WideString); safecall;
    function Get_PluginToExecuteButtonCaption: WideString; safecall;
    procedure Set_PluginToExecuteButtonCaption(const Value: WideString); safecall;
    function Get_CompileDenyRead: WordBool; safecall;
    procedure Set_CompileDenyRead(Value: WordBool); safecall;
    function Get_CompileDenyWrite: WordBool; safecall;
    procedure Set_CompileDenyWrite(Value: WordBool); safecall;
    function Get_CompileDenyOLE: WordBool; safecall;
    procedure Set_CompileDenyOLE(Value: WordBool); safecall;
    function Get_IsInherited: WordBool; safecall;
    procedure Set_IsInherited(Value: WordBool); safecall;
    function Get_InheritAction: InheritActions; safecall;
    procedure Set_InheritAction(Value: InheritActions); safecall;
    function Get_CalculateBeforeInherit: WordBool; safecall;
    procedure Set_CalculateBeforeInherit(Value: WordBool); safecall;
    function Get_InheritPullFrom: WideString; safecall;
    procedure Set_InheritPullFrom(const Value: WideString); safecall;
    function Get_ImageTransparent: WordBool; safecall;
    procedure Set_ImageTransparent(Value: WordBool); safecall;
    function Get_ScriptType: ScriptTypes; safecall;
    procedure Set_ScriptType(Value: ScriptTypes); safecall;
    function Get_ScriptLanguage: ScriptLanguages; safecall;
    procedure Set_ScriptLanguage(Value: ScriptLanguages); safecall;
    function Get_ScriptParameters: WideString; safecall;
    procedure Set_ScriptParameters(const Value: WideString); safecall;
    function ExecuteScript(const ParamList: WideString): OleVariant; safecall;
    function Get_SliderMax: Integer; safecall;
    procedure Set_SliderMax(Value: Integer); safecall;
    function Get_SliderMin: Integer; safecall;
    procedure Set_SliderMin(Value: Integer); safecall;
    function Get_SliderTickFrequency: Integer; safecall;
    procedure Set_SliderTickFrequency(Value: Integer); safecall;
    function Get_SliderShowTicks: WordBool; safecall;
    procedure Set_SliderShowTicks(Value: WordBool); safecall;
    procedure EditScript; safecall;
    function GUID: WideString; safecall;
    function Get_ParseFormula: WordBool; safecall;
    procedure Set_ParseFormula(Value: WordBool); safecall;
    function Get_FixedSimpleList: WordBool; safecall;
    procedure Set_FixedSimpleList(Value: WordBool); safecall;
    property Name: WideString read Get_Name write Set_Name;
    property Formula: WideString read Get_Formula write Set_Formula;
    property Units: WideString read Get_Units write Set_Units;
    property InputUnits: WideString read Get_InputUnits write Set_InputUnits;
    property Group: WideString read Get_Group write Set_Group;
    property IsInput: WordBool read Get_IsInput write Set_IsInput;
    property InputType: InputTypes read Get_InputType write Set_InputType;
    property UserLocked: WordBool read Get_UserLocked write Set_UserLocked;
    property UserHidden: WordBool read Get_UserHidden write Set_UserHidden;
    property Adjust: WideString read Get_Adjust write Set_Adjust;
    property DecimalPlaces: Integer read Get_DecimalPlaces write Set_DecimalPlaces;
    property InputCondition: WideString read Get_InputCondition write Set_InputCondition;
    property Expression: WordBool read Get_Expression write Set_Expression;
    property ListType: ListTypes read Get_ListType write Set_ListType;
    property SimpleList: WideString read Get_SimpleList write Set_SimpleList;
    property List: WideString read Get_List write Set_List;
    property ListResultColumn: WideString read Get_ListResultColumn write Set_ListResultColumn;
    property TreeList: WideString read Get_TreeList write Set_TreeList;
    property ListFromProperty: WordBool read Get_ListFromProperty write Set_ListFromProperty;
    property ListShowSearch: WordBool read Get_ListShowSearch write Set_ListShowSearch;
    property ListColumnAutoWidth: WordBool read Get_ListColumnAutoWidth write Set_ListColumnAutoWidth;
    property ListVisibleColumnsInDropdown: WideString read Get_ListVisibleColumnsInDropdown write Set_ListVisibleColumnsInDropdown;
    property ListPropertiesToSet: WideString read Get_ListPropertiesToSet write Set_ListPropertiesToSet;
    property ListShowOnlyTypes: WideString read Get_ListShowOnlyTypes write Set_ListShowOnlyTypes;
    property ListReturnFullPath: WordBool read Get_ListReturnFullPath write Set_ListReturnFullPath;
    property ListShow1Level: WordBool read Get_ListShow1Level write Set_ListShow1Level;
    property PluginToExecute: WideString read Get_PluginToExecute write Set_PluginToExecute;
    property PluginToExecuteButtonCaption: WideString read Get_PluginToExecuteButtonCaption write Set_PluginToExecuteButtonCaption;
    property CompileDenyRead: WordBool read Get_CompileDenyRead write Set_CompileDenyRead;
    property CompileDenyWrite: WordBool read Get_CompileDenyWrite write Set_CompileDenyWrite;
    property CompileDenyOLE: WordBool read Get_CompileDenyOLE write Set_CompileDenyOLE;
    property IsInherited: WordBool read Get_IsInherited write Set_IsInherited;
    property InheritAction: InheritActions read Get_InheritAction write Set_InheritAction;
    property CalculateBeforeInherit: WordBool read Get_CalculateBeforeInherit write Set_CalculateBeforeInherit;
    property InheritPullFrom: WideString read Get_InheritPullFrom write Set_InheritPullFrom;
    property ImageTransparent: WordBool read Get_ImageTransparent write Set_ImageTransparent;
    property ScriptType: ScriptTypes read Get_ScriptType write Set_ScriptType;
    property ScriptLanguage: ScriptLanguages read Get_ScriptLanguage write Set_ScriptLanguage;
    property ScriptParameters: WideString read Get_ScriptParameters write Set_ScriptParameters;
    property SliderMax: Integer read Get_SliderMax write Set_SliderMax;
    property SliderMin: Integer read Get_SliderMin write Set_SliderMin;
    property SliderTickFrequency: Integer read Get_SliderTickFrequency write Set_SliderTickFrequency;
    property SliderShowTicks: WordBool read Get_SliderShowTicks write Set_SliderShowTicks;
    property ParseFormula: WordBool read Get_ParseFormula write Set_ParseFormula;
    property FixedSimpleList: WordBool read Get_FixedSimpleList write Set_FixedSimpleList;
  end;

// *********************************************************************//
// DispIntf:  IPropertyObjectDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {325EA379-B734-4500-87BA-3C1D25F4D90A}
// *********************************************************************//
  IPropertyObjectDisp = dispinterface
    ['{325EA379-B734-4500-87BA-3C1D25F4D90A}']
    property Name: WideString dispid 201;
    property Formula: WideString dispid 202;
    property Units: WideString dispid 203;
    property InputUnits: WideString dispid 204;
    property Group: WideString dispid 205;
    function PropertyType: WideString; dispid 206;
    function ResultAsString: WideString; dispid 207;
    function ResultAsFloat: Double; dispid 208;
    function ResultAsVariant: OleVariant; dispid 209;
    function ResultAsInteger: Integer; dispid 210;
    function SystemHidden: WordBool; dispid 211;
    function SystemLocked: WordBool; dispid 212;
    property IsInput: WordBool dispid 213;
    property InputType: InputTypes dispid 214;
    property UserLocked: WordBool dispid 215;
    property UserHidden: WordBool dispid 216;
    property Adjust: WideString dispid 217;
    property DecimalPlaces: Integer dispid 218;
    property InputCondition: WideString dispid 219;
    property Expression: WordBool dispid 220;
    property ListType: ListTypes dispid 221;
    property SimpleList: WideString dispid 222;
    property List: WideString dispid 223;
    property ListResultColumn: WideString dispid 224;
    property TreeList: WideString dispid 225;
    property ListFromProperty: WordBool dispid 226;
    property ListShowSearch: WordBool dispid 227;
    function MeetsInputCondition: WordBool; dispid 228;
    property ListColumnAutoWidth: WordBool dispid 229;
    property ListVisibleColumnsInDropdown: WideString dispid 230;
    property ListPropertiesToSet: WideString dispid 231;
    property ListShowOnlyTypes: WideString dispid 232;
    property ListReturnFullPath: WordBool dispid 233;
    property ListShow1Level: WordBool dispid 234;
    property PluginToExecute: WideString dispid 235;
    property PluginToExecuteButtonCaption: WideString dispid 236;
    property CompileDenyRead: WordBool dispid 237;
    property CompileDenyWrite: WordBool dispid 238;
    property CompileDenyOLE: WordBool dispid 239;
    property IsInherited: WordBool dispid 240;
    property InheritAction: InheritActions dispid 241;
    property CalculateBeforeInherit: WordBool dispid 242;
    property InheritPullFrom: WideString dispid 243;
    property ImageTransparent: WordBool dispid 244;
    property ScriptType: ScriptTypes dispid 245;
    property ScriptLanguage: ScriptLanguages dispid 246;
    property ScriptParameters: WideString dispid 247;
    function ExecuteScript(const ParamList: WideString): OleVariant; dispid 248;
    property SliderMax: Integer dispid 249;
    property SliderMin: Integer dispid 250;
    property SliderTickFrequency: Integer dispid 251;
    property SliderShowTicks: WordBool dispid 252;
    procedure EditScript; dispid 253;
    function GUID: WideString; dispid 254;
    property ParseFormula: WordBool dispid 255;
    property FixedSimpleList: WordBool dispid 256;
  end;

// *********************************************************************//
// Interface: IPoint
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {9661175A-6FB8-4D72-A907-7551AAF03B6B}
// *********************************************************************//
  IPoint = interface(IDispatch)
    ['{9661175A-6FB8-4D72-A907-7551AAF03B6B}']
    function Get_X: Single; safecall;
    procedure Set_X(Value: Single); safecall;
    function Get_Y: Single; safecall;
    procedure Set_Y(Value: Single); safecall;
    function Get_PointType: PointTypes; safecall;
    procedure Set_PointType(Value: PointTypes); safecall;
    property X: Single read Get_X write Set_X;
    property Y: Single read Get_Y write Set_Y;
    property PointType: PointTypes read Get_PointType write Set_PointType;
  end;

// *********************************************************************//
// DispIntf:  IPointDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {9661175A-6FB8-4D72-A907-7551AAF03B6B}
// *********************************************************************//
  IPointDisp = dispinterface
    ['{9661175A-6FB8-4D72-A907-7551AAF03B6B}']
    property X: Single dispid 201;
    property Y: Single dispid 202;
    property PointType: PointTypes dispid 203;
  end;

// *********************************************************************//
// Interface: ISelectionList
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {A974B0EA-8A2A-441E-96E2-6DFC79514411}
// *********************************************************************//
  ISelectionList = interface(IDispatch)
    ['{A974B0EA-8A2A-441E-96E2-6DFC79514411}']
    function Get_Items(Index: Integer): IItem; safecall;
    function Get_Count: Integer; safecall;
    property Items[Index: Integer]: IItem read Get_Items; default;
    property Count: Integer read Get_Count;
  end;

// *********************************************************************//
// DispIntf:  ISelectionListDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {A974B0EA-8A2A-441E-96E2-6DFC79514411}
// *********************************************************************//
  ISelectionListDisp = dispinterface
    ['{A974B0EA-8A2A-441E-96E2-6DFC79514411}']
    property Items[Index: Integer]: IItem readonly dispid 0; default;
    property Count: Integer readonly dispid 202;
  end;

// *********************************************************************//
// Interface: ILine
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {AED4DE6B-9639-4397-BFFB-B94BC73F7FC9}
// *********************************************************************//
  ILine = interface(IDispatch)
    ['{AED4DE6B-9639-4397-BFFB-B94BC73F7FC9}']
    function Get_Point1: IPoint; safecall;
    function Get_Point2: IPoint; safecall;
    property Point1: IPoint read Get_Point1;
    property Point2: IPoint read Get_Point2;
  end;

// *********************************************************************//
// DispIntf:  ILineDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {AED4DE6B-9639-4397-BFFB-B94BC73F7FC9}
// *********************************************************************//
  ILineDisp = dispinterface
    ['{AED4DE6B-9639-4397-BFFB-B94BC73F7FC9}']
    property Point1: IPoint readonly dispid 201;
    property Point2: IPoint readonly dispid 202;
  end;

// *********************************************************************//
// The Class CoPlanSwift provides a Create and CreateRemote method to          
// create instances of the default interface IPlanSwift exposed by              
// the CoClass PlanSwift. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoPlanSwift = class
    class function Create: IPlanSwift;
    class function CreateRemote(const MachineName: string): IPlanSwift;
  end;

// *********************************************************************//
// The Class CoItem provides a Create and CreateRemote method to          
// create instances of the default interface IItem exposed by              
// the CoClass Item. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoItem = class
    class function Create: IItem;
    class function CreateRemote(const MachineName: string): IItem;
  end;

// *********************************************************************//
// The Class CoPropertyObject provides a Create and CreateRemote method to          
// create instances of the default interface IPropertyObject exposed by              
// the CoClass PropertyObject. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoPropertyObject = class
    class function Create: IPropertyObject;
    class function CreateRemote(const MachineName: string): IPropertyObject;
  end;

// *********************************************************************//
// The Class CoSelectionList provides a Create and CreateRemote method to          
// create instances of the default interface ISelectionList exposed by              
// the CoClass SelectionList. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoSelectionList = class
    class function Create: ISelectionList;
    class function CreateRemote(const MachineName: string): ISelectionList;
  end;

implementation

uses System.Win.ComObj;

class function CoPlanSwift.Create: IPlanSwift;
begin
  Result := CreateComObject(CLASS_PlanSwift) as IPlanSwift;
end;

class function CoPlanSwift.CreateRemote(const MachineName: string): IPlanSwift;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_PlanSwift) as IPlanSwift;
end;

class function CoItem.Create: IItem;
begin
  Result := CreateComObject(CLASS_Item) as IItem;
end;

class function CoItem.CreateRemote(const MachineName: string): IItem;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_Item) as IItem;
end;

class function CoPropertyObject.Create: IPropertyObject;
begin
  Result := CreateComObject(CLASS_PropertyObject) as IPropertyObject;
end;

class function CoPropertyObject.CreateRemote(const MachineName: string): IPropertyObject;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_PropertyObject) as IPropertyObject;
end;

class function CoSelectionList.Create: ISelectionList;
begin
  Result := CreateComObject(CLASS_SelectionList) as ISelectionList;
end;

class function CoSelectionList.CreateRemote(const MachineName: string): ISelectionList;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_SelectionList) as ISelectionList;
end;

end.
