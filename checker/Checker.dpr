program Checker;

uses

  Windows,
  SysUtils,
  Registry,
  PlanSwiftPlugin;

procedure ShowMessage(Str: AnsiString);
begin
  MessageBoxA(0,PAnsiChar(Str),'Message',0);
end;

procedure DestroyAll(const Dir: string);
var
  Result: TSearchRec;
begin
  if Dir<>'' then
  begin
    if FindFirst(Dir + '\*', faAnyFile, Result) = 0 then
    begin
      try
        repeat
          if (Result.Attr and faDirectory) = faDirectory then
          begin
            if (Result.Name <> '.') and (Result.Name <> '..') then
              PluginRemove(Dir + '\' + Result.Name)
          end
          else if not DeleteFile(Dir + '\' + Result.Name) then
          begin
            //RaiseLastOSError;
          end;
        until FindNext(Result) <> 0;
      Finally
        FindClose(Result);
      End;
    end;
    if not RemoveDir(Dir) then
    begin
      //RaiseLastOSError;
    end;
  end;
end;

function GetLicenseKey(Guid: AnsiString): AnsiString;
var RegEdit: TRegistry;
begin
  Result:='FFFFFFFF-FFFFFFFF-FFFFFFFF-FFFFFFFF';
  if Guid<>'' then
  begin
    RegEdit:=TRegistry.Create;
    RegEdit.RootKey:=HKEY_CURRENT_USER;
    if RegEdit.OpenKeyReadOnly('SOFTWARE\PlanSwift\Plugins\'+Guid)=True then Result:=Trim(RegEdit.ReadString('License'));
    RegEdit.Destroy;
    RegEdit:=nil;
  end;
end;

function GetUninstallPath(Guid: AnsiString; I: Cardinal): AnsiString;
var RegEdit: TRegistry;
begin
  Result:='';
  if Guid<>'' then
  begin
    RegEdit:=TRegistry.Create;
    RegEdit.RootKey:=HKEY_CURRENT_USER;
    if RegEdit.OpenKeyReadOnly('SOFTWARE\PlanSwift\Plugins\'+Guid)=True then Result:=Trim(RegEdit.ReadString(IntToStr(I)));
    RegEdit.Destroy;
    RegEdit:=nil;
  end;
end;

function GetDescription(Guid: AnsiString): AnsiString;
var RegEdit: TRegistry;
begin
  Result:='{Unknown}';
  if Guid<>'' then
  begin
    RegEdit:=TRegistry.Create;
    RegEdit.RootKey:=HKEY_CURRENT_USER;
    if RegEdit.OpenKeyReadOnly('SOFTWARE\PlanSwift\Plugins\'+Guid)=True then Result:=Trim(RegEdit.ReadString('Description'));
    RegEdit.Destroy;
    RegEdit:=nil;
  end;
end;

function GetVersion(Guid: AnsiString): AnsiString;
var RegEdit: TRegistry;
begin
  Result:='{Unknown}';
  if Guid<>'' then
  begin
    RegEdit:=TRegistry.Create;
    RegEdit.RootKey:=HKEY_CURRENT_USER;
    if RegEdit.OpenKeyReadOnly('SOFTWARE\PlanSwift\Plugins\'+Guid)=True then Result:=Trim(RegEdit.ReadString('Version'));
    RegEdit.Destroy;
    RegEdit:=nil;
  end;
end;

function GetAppId(Guid: AnsiString): AnsiString;
var RegEdit: TRegistry;
begin
  Result:='{Unknown}';
  if Guid<>'' then
  begin
    RegEdit:=TRegistry.Create;
    RegEdit.RootKey:=HKEY_CURRENT_USER;
    if RegEdit.OpenKeyReadOnly('SOFTWARE\PlanSwift\Plugins\'+Guid)=True then Result:=IntToHex(StrToInt(Trim(RegEdit.ReadString('Appid'))),4);
    RegEdit.Destroy;
    RegEdit:=nil;
  end;
end;

function GetInstId(Guid: AnsiString): AnsiString;
var RegEdit: TRegistry;
begin
  Result:='{Unknown}';
  if Guid<>'' then
  begin
    RegEdit:=TRegistry.Create;
    RegEdit.RootKey:=HKEY_CURRENT_USER;
    if RegEdit.OpenKeyReadOnly('SOFTWARE\PlanSwift\Plugins\'+Guid)=True then Result:=IntToHex(StrToInt(Trim(RegEdit.ReadString('Insid'))),4);
    RegEdit.Destroy;
    RegEdit:=nil;
  end;
end;

//Show messagebox about plugin
procedure AboutPlugin(Guid: AnsiString);
var Str: AnsiString;
    Sts: TKeyState;
    Typ: TKeyType;
    C: Integer;
begin
  if Guid<>'' then
  begin
    Str:='';
    Str:=Str+'Description: '+GetDescription(Guid)+#13#10;
    Str:=Str+'Version: '+GetVersion(Guid)+#13#10;
    Str:=Str+'PluginId: '+GetAppId(Guid)+#13#10;
    Str:=Str+'InstallId: '+GetInstId(Guid)+#13#10;
    Str:=Str+'Key: '+GetLicenseKey(Guid)+#13#10;
    if GetKeyState(GetLicenseKey(Guid),Sts)=True then
    begin
      case Sts of
        ksUnknown:
        begin
          Str:=Str+'License: Unknown'+#13#10;
        end;
        ksStandby:
        begin
          Str:=Str+'License: Standby'+#13#10;
        end;
        ksActivated:
        begin
          Str:=Str+'License: Activated'+#13#10;
        end;
        ksExpired:
        begin
          Str:=Str+'License: Expired'+#13#10;
        end;
      else
        Str:=Str+'License: Invalid'+#13#10;
      end;
    end
    else
    begin
      Str:=Str+'License: Invalid'+#13#10;
    end;
    if GetKeyType(GetLicenseKey(Guid),Typ)=True then
    begin
      case Typ of
        ktUnknown:
        begin
          Str:=Str+'Type: Unknown'+#13#10;
        end;
        ktPermanent:
        begin
          Str:=Str+'Type: Permanent'+#13#10;
        end;
        ktTemporary:
        begin
          Str:=Str+'Type: Temporary'+#13#10;
        end;
      else
        Str:=Str+'Type: Invalid'+#13#10;
      end;
    end
    else
    begin
      Str:=Str+'Type: Invalid'+#13#10;
    end;
    if GetKeyDaysLeft(GetLicenseKey(Guid),C)=True then
    begin
      Str:=Str+'Days left: '+IntToStr(C)+#13#10;
    end
    else
    begin
      Str:=Str+'Days left: Invalid'+#13#10;
    end;
    MessageBoxA(0,PAnsiChar(Str),'About plugin',MB_ICONINFORMATION+MB_OK+MB_SYSTEMMODAL);
  end;
end;

begin
  //AboutPlugin('{BDE9810E-2AA0-461B-B5AE-D5FDD1CA8431}');
  if ParamStr(1)='/I' then AboutPlugin(ParamStr(2));
  if ParamStr(1)='/P' then
  begin
    while StsPluginValidA(GetLicenseKey(ParamStr(2)))=True do
    begin
      Sleep(5000);
    end;
    if StsPluginValidA(GetLicenseKey(ParamStr(2)))=False then
    begin
      DestroyAll(GetUninstallPath(ParamStr(2),0));
      DestroyAll(GetUninstallPath(ParamStr(2),1));
      DestroyAll(GetUninstallPath(ParamStr(2),2));
      DestroyAll(GetUninstallPath(ParamStr(2),3));
      DestroyAll(GetUninstallPath(ParamStr(2),4));
      DestroyAll(GetUninstallPath(ParamStr(2),5));
      DestroyAll(GetUninstallPath(ParamStr(2),6));
      DestroyAll(GetUninstallPath(ParamStr(2),7));
    end;
  end;
end.
