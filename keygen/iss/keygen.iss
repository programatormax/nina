;KeyGen install script

#define ReleaseDir GetDateTimeString('dd/mm/yyyy', '-', ':')

[Setup]
AppName=KeyGen
AppVerName=KeyGen v1.1
DefaultDirName={pf}\KeyGen
DefaultGroupName=KeyGen
AllowNoIcons=false
OutputDir=..\..\build\KeyGen\{#ReleaseDir}
OutputBaseFilename=KeyGenInstall
Compression=lzma
SolidCompression=true
WizardImageFile=.\wizard.bmp
AppID={{E3D2066C-7C10-47EB-8FC7-0C26BBBC9E3C}}
ShowLanguageDialog=no
WizardImageStretch=false
DisableProgramGroupPage=true
VersionInfoTextVersion=1.0.0.0
VersionInfoVersion=1.0.0.0
VersionInfoCompany=KeyGen
VersionInfoCopyright=All rights reserved 2022
VersionInfoDescription=KeyGen
DisableWelcomePage=no
UsePreviousLanguage=no

[Tasks]

Name: desktopicon; Description: {cm:CreateDesktopIcon}; GroupDescription: {cm:AdditionalIcons}; Flags: unchecked

[Icons]

Name: "{commondesktop}\KeyGen"; Filename: "{app}\KeyGenerator.exe"; Tasks: desktopicon; IconIndex: 0;

[Files]
          
Source: "..\bin\KeyGenerator.exe"; DestDir: {app}\; Flags: ignoreversion;
Source: "..\bin\SampleDataBase.dbkey"; DestDir: {app}\; Flags: ignoreversion;
                     

                                           

                                                                       
                                                     

