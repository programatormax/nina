unit KeyGen;

interface

uses

  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ComCtrls, Vcl.StdCtrls, DateUtils,
  Vcl.Grids, Vcl.ValEdit, System.ImageList, Vcl.ImgList, Vcl.Menus, SuperObject;

type

  TKey = packed record
    Prefx: Byte;     //Auto - Prefix for identify
    RrdId: Byte;     //Auto - Random xor index
    Crc: Byte;       //Auto - Check sum
    Flag: Byte;      //Must be filled - Key's flags
    AppId: Word;     //Must be filled - Application id
    InsId: Word;     //Must be filled - Application install id
    Start: Cardinal; //Must be filled - Start range time
    Stop: Cardinal;  //Must be filled - Stop range time
  end;

  TForm1 = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    DateTimePicker1: TDateTimePicker;
    Label2: TLabel;
    DateTimePicker2: TDateTimePicker;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    Label3: TLabel;
    Edit1: TEdit;
    Label4: TLabel;
    Edit2: TEdit;
    Edit3: TEdit;
    Label5: TLabel;
    Button1: TButton;
    Button2: TButton;
    Memo1: TMemo;
    GroupBox2: TGroupBox;
    TreeView1: TTreeView;
    ValueListEditor1: TValueListEditor;
    PopupMenu1: TPopupMenu;
    ImageList1: TImageList;
    Add1: TMenuItem;
    Delete1: TMenuItem;
    Delete2: TMenuItem;
    User1: TMenuItem;
    Key1: TMenuItem;
    Edit4: TMenuItem;
    MainMenu1: TMainMenu;
    File1: TMenuItem;
    New1: TMenuItem;
    Open1: TMenuItem;
    Save1: TMenuItem;
    Saveas1: TMenuItem;
    N1: TMenuItem;
    Exit1: TMenuItem;
    Help1: TMenuItem;
    About1: TMenuItem;
    SaveDialog1: TSaveDialog;
    OpenDialog1: TOpenDialog;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure TreeView1Click(Sender: TObject);
    procedure Delete2Click(Sender: TObject);
    procedure User1Click(Sender: TObject);
    procedure Key1Click(Sender: TObject);
    procedure Edit4Click(Sender: TObject);
    procedure New1Click(Sender: TObject);
    procedure Exit1Click(Sender: TObject);
    procedure Saveas1Click(Sender: TObject);
    procedure Save1Click(Sender: TObject);
    procedure Open1Click(Sender: TObject);
    procedure About1Click(Sender: TObject);
  private
    FFileName: AnsiString;
  public
    { Public declarations }
  end;

var

  Form1: TForm1;

implementation

{$R *.dfm}

//------------------------------------------------------------------------------
// Key format
//------------------------------------------------------------------------------

//byte CRC8(const byte *data, byte len) {
//  byte crc = 0x00;
//  while (len--) {
//    byte extract = *data++;
//    for (byte tempI = 8; tempI; tempI--) {
//      byte sum = (crc ^ extract) & 0x01;
//      crc >>= 1;
//      if (sum) {
//        crc ^= 0x8C;
//      }
//      extract >>= 1;
//    }
//  }
//  return crc;
//}

//Check sum
function CRC8(data: pbyte; len: byte): byte;
var n, crc, extract,tempI,sum : byte;
begin
  crc := 0;
  for n := len downto 1 do
  begin
    extract := data^;
    inc (data);
    for tempI := 8 downto 1 do
    begin
      sum := (crc xor extract) and 1;
      crc := crc shr 1;
      if sum = 1 then crc := crc xor $8C;
      extract := extract shr 1;
    end;
  end;
  exit(crc);
end;

//Convert Key To String
function KeyToStr(var Key: TKey): AnsiString;
var Buf: array [0..15] of Byte;
      r: Byte;
     n: Integer;
begin
  Result:='';
  randomize;
  Key.Prefx:=$0A;
  Key.RrdId:=Random(255);
  r:=Key.RrdId;
  Buf[0]:=Key.Prefx;
  Buf[1]:=Key.RrdId;
  Buf[2]:=0;
  Buf[3]:=Key.Flag;
  Move(Key.AppId,Buf[4],2);
  Move(Key.InsId,Buf[6],2);
  Move(Key.Start,Buf[8],4);
  Move(Key.Stop,Buf[12],4);
  Buf[2]:=CRC8(@Buf[0],16);
  for n:=Low(Buf) to High(Buf) do
  begin
    Buf[n]:=Buf[n] xor Byte(r+n*n);
  end;
  Buf[1]:=r;
  for n:=Low(Buf) to High(Buf) do
  begin
    Result:=Result+IntToHex(Buf[n],2);
  end;
  Result:=Copy(Result,1,8)+'-'+Copy(Result,9,8)+'-'+Copy(Result,17,8)+'-'+Copy(Result,25,8);
end;

//Convert Str To Key
function StrToKey(Str: AnsiString; var Key: TKey): Boolean;
var Buf: array [0..15] of Byte;
    L: Cardinal;
    Q: Cardinal;
    c: Byte;
    K: AnsiString;
    r: Byte;
    n: Integer;
begin
  Result:=False;
  K:=Trim(Str);
  if (Length(K)=35) and
     (K[9]='-') and
     (K[18]='-') and
     (K[27]='-') then
  begin
    K:=StringReplace(K,'-','',[rfReplaceAll,rfIgnoreCase]);
    L:=1;
    Q:=0;
    while L<32 do
    begin
      Buf[Q]:=StrToInt('$'+K[L]+K[L+1]);
      L:=L+2;
      Q:=Q+1;
    end;
    r:=Buf[1];
    for n:=Low(Buf) to High(Buf) do
    begin
      Buf[n]:=Buf[n] xor Byte(r+n*n);
    end;
    Buf[1]:=r;
    c:=Buf[2];
    Buf[2]:=0;
    if CRC8(@Buf[0],16)=C then
    begin
      if Buf[0]=$0A then
      begin
        Key.Prefx:=Buf[0];
        Key.RrdId:=Buf[1];
        Key.Crc:=Buf[2];
        Key.Flag:=Buf[3];
        Key.AppId:=(Buf[4]+(Buf[5]*256));
        Key.InsId:=(Buf[6]+(Buf[7]*256));
        Move(Buf[8],Key.Start,4);
        Move(Buf[12],Key.Stop,4);
        Result:=True;
      end;
    end;
  end;
end;

function GetKeyImageIndex(S: AnsiString): Integer;
var K: TKey;
    Dtn: Cardinal;
begin
  Result:=4;
  if StrToKey(Trim(S),K)=True then
  begin
    if K.Flag=1 then
    begin
      Result:=2;
    end
    else
    begin
      Dtn:=DateTimeToUnix(DateOf(Now));
      if (K.Start<=Dtn) and (K.Stop>=Dtn) then
      begin
        Result:=2;
      end
      else
      begin
        if Dtn>K.Stop then
        begin
          Result:=4;
        end
        else
        begin
          Result:=3;
        end;
      end;
    end;
  end;
end;

function JsonToNode(Obj: ISuperObject; Parent: TTreeNode; List: TTreeView): TTreeNode;
var q: Integer;
    Node: TTreeNode;
begin
  if Assigned(Parent)=True then Node:=List.Items.AddChild(Parent,Obj.S['name']) else Node:=List.Items.Add(nil,Obj.S['name']);
  Node.ImageIndex:=Obj.I['image'];
  Node.SelectedIndex:=Obj.I['select'];
  Node.StateIndex:=Obj.I['state'];
  if Node.ImageIndex>0 then
  begin
    Node.ImageIndex:=GetKeyImageIndex(Node.Text);
    Node.SelectedIndex:=GetKeyImageIndex(Node.Text);
  end;
  //Node.Data:=TNotebookNode.Create;
  //TNotebookNode(Node.Data).Desc:=Obj.S['desc'];
  //TNotebookNode(Node.Data).Icon:=Obj.I['image'];
  if (Assigned(Obj.A['child'])=True) and
     (Assigned(Obj.A['child'].O[0])=True) then
  begin
    q:=0;
    while (Assigned(Obj.A['child'].O[q])=True) do
    begin
      JsonToNode(Obj.A['child'].O[q],Node,List);
      q:=q+1;
    end;
  end;
end;

function NodeToJson(Node: TTreeNode): String;
var n: Integer;
begin
  if Assigned(Node)=True then
  begin
    Result:='';
    Result:=Result+'{';
    Result:=Result+'"name": "'+Node.Text+'",';
    Result:=Result+'"image": '+IntToStr(Node.ImageIndex)+',';
    Result:=Result+'"select": '+IntToStr(Node.SelectedIndex)+',';
    Result:=Result+'"state": '+IntToStr(Node.StateIndex)+',';
    if Assigned(Node.Data)=True then
    begin
      //Result:=Result+'"desc": "'+TNotebookNode(Node.Data).Desc+'",';
      Result:=Result+'"desc": "",';
    end
    else
    begin
     Result:=Result+'"desc": "",';
    end;
    if (Node.HasChildren=True) and (Node.Count>0) then
    begin
      Result:=Result+'"child": [';
      for n:=0 to Node.Count-1 do
      begin
        if n<Node.Count then Result:=Result+NodeToJson(Node.Item[n])+',' else Result:=Result+NodeToJson(Node.Item[n]);
      end;
      Result:=Result+']';
    end
    else
    begin
      Result:=Result+'"child": []';
    end;
    Result:=Result+'}';
  end
  else
  begin
    Result:='{}';
  end;
end;

function TreeToObj(List: TTreeView): ISuperObject;
var n: Integer;
    R: String;
begin
  R:='{';
  if (Assigned(List)=True) and (List.Items.Count>0) then
  begin
    R:=R+'"dbkeydatabase": [';
    for n:=0 to List.Items.Count-1 do
    begin
      if Assigned(List.Items.Item[n].Parent)=False then
      begin
        if n<List.Items.Count then R:=R+NodeToJson(List.Items.Item[n])+',' else R:=R+NodeToJson(List.Items.Item[n]);
      end;
    end;
    R:=R+']';
  end
  else
  begin
    R:=R+'"dbkeydatabase": []';
  end;
  R:=R+'}';
  Result:=SO(R);
end;

function ObjToTree(Obj: ISuperObject; List: TTreeView): Boolean;
var q: Integer;
begin
  Result:=False;
  if (Assigned(List)=True) and
     (Assigned(Obj)=True) and
     (Assigned(Obj.O['dbkeydatabase'])=True) then
  begin
    List.Items.BeginUpdate;
    List.Items.Clear;
    q:=0;
    while Assigned(Obj.A['dbkeydatabase'].O[q]) do
    begin
      JsonToNode(Obj.A['dbkeydatabase'].O[q],nil,List);
      q:=q+1;
    end;
    List.Items.EndUpdate;
    Result:=True;
  end;
end;

//Button Generate
procedure TForm1.About1Click(Sender: TObject);
var Str: String;
begin
  Str:='KeyGen v1.1';
  Application.MessageBox(PWideChar(Str),'About',MB_OK+MB_ICONINFORMATION);
end;

procedure TForm1.Button1Click(Sender: TObject);
var Key: TKey;
begin
  if Edit1.Text='' then raise Exception.Create('Application id parameter must be filled');
  if Length(Trim(Edit1.Text))<>4 then raise Exception.Create('Application id parameter length must be 4 but not '+IntToStr(Length(Trim(Edit1.Text))));
  if Edit2.Text='' then raise Exception.Create('Install id parameter must be filled');
  if Length(Trim(Edit2.Text))<>4 then raise Exception.Create('Install id parameter length must be 4 but not '+IntToStr(Length(Trim(Edit2.Text))));
  if (RadioButton1.Checked=False) and (RadioButton2.Checked=False) then raise Exception.Create('Permanent or Temporary types not defined');
  if DateTimeToUnix(DateTimePicker1.DateTime)>DateTimeToUnix(DateTimePicker2.DateTime) then raise Exception.Create('Start date must be less then stop date');
  Key.AppId:=StrToInt('$'+Trim(Edit1.Text));
  Key.InsId:=StrToInt('$'+Trim(Edit2.Text));
  if RadioButton2.Checked=True then
  begin
    Key.Flag:=$01;
    Key.Start:=Random(MaxDword);
    Key.Stop:=Random(MaxDword);
  end
  else
  begin
    Key.Flag:=$00;
    Key.Start:=DateTimeToUnix(DateTimePicker1.Date);
    Key.Stop:=DateTimeToUnix(DateTimePicker2.Date);
  end;
  Edit3.Text:=KeyToStr(Key);
  Button2Click(nil);
end;

//Button Check
procedure TForm1.Button2Click(Sender: TObject);
var Key: TKey;
    Str: AnsiString;
    Dtn: Cardinal;
begin
  if StrToKey(Edit3.Text,Key)=True then
  begin
    Str:=Str+'Application id: '+IntToHex(Key.AppId,4)+#13#10;
    Str:=Str+'Install id: '+IntToHex(Key.InsId,4)+#13#10;
    if Key.Flag=1 then
    begin
      Str:=Str+'Type: PERMANENT'+#13#10;
      Str:=Str+'Status: [ ACTIVATED ]'+#13#10;
      Edit3.Color:=clGreen;
    end
    else
    begin
      Str:=Str+'Type: TEMPORARY'+#13#10;
      Dtn:=DateTimeToUnix(DateOf(Now));
      if (Key.Start<=Dtn) and (Key.Stop>=Dtn) then
      begin
        Str:=Str+'Status: [ ACTIVATED ]'+#13#10;
        Edit3.Color:=clGreen;
      end
      else
      begin
        if Dtn>Key.Stop then
        begin
          Str:=Str+'Status: [ EXPIRED ]'+#13#10;
        end
        else
        begin
          Str:=Str+'Status: [ NOT ACTIVATED ]'+#13#10;
        end;
        Edit3.Color:=clYellow;
      end;
      Str:=Str+'Current date: '+DateTimeToStr(DateOf(Now))+#13#10;
      Str:=Str+'Start date: '+DateTimeToStr(UnixToDateTime(Key.Start))+#13#10;
      Str:=Str+'Stop date: '+DateTimeToStr(UnixToDateTime(Key.Stop))+#13#10;
      Str:=Str+'Days total: '+IntToStr(Round(((Key.Stop-Key.Start)/3600)/24));
    end;
  end
  else
  begin
    Str:='Key: '+Trim(Edit3.Text)+#13#10;
    Str:=Str+'Type: INVALID';
    Edit3.Color:=clRed;
  end;
  Memo1.Lines.Clear;
  Memo1.Lines.Text:=Str;
end;

procedure TForm1.Delete2Click(Sender: TObject);
begin
  if Assigned(TreeView1.Selected)=True then
  begin
    if Application.MessageBox('Delete this record',PWideChar(TreeView1.Selected.Text),MB_ICONQUESTION + MB_YESNO)=IDYES then TreeView1.Selected.Delete;
  end;
end;

procedure TForm1.Edit4Click(Sender: TObject);
begin
  if (Assigned(TreeView1.Selected)=True) and
     (TreeView1.Selected.ImageIndex=0) then
  begin
    TreeView1.Selected.Text:=StringReplace(Trim(Vcl.Dialogs.InputBox('User','Enter email',StringReplace(Trim(TreeView1.Selected.Text),' ','',[rfReplaceAll]))),' ','',[rfReplaceAll]);
  end;
end;

procedure TForm1.Exit1Click(Sender: TObject);
begin
  Halt;
end;

//On Show Window
procedure TForm1.FormShow(Sender: TObject);
var Dtm: TDateTime;
begin
  Dtm:=Now;
  DateTimePicker1.Date:=DateOf(Dtm);
  DateTimePicker2.Date:=DateOf(Dtm);
  FFileName:='C:\NewKeysDB.dbkey';
  Form1.Caption:='KeyGen v1.1 - '+FFileName;
end;

procedure TForm1.Key1Click(Sender: TObject);
var N: TTreeNode;
    K: TKey;
begin
  if Assigned(TreeView1.Selected)=True then
  begin
    N:=nil;
    if TreeView1.Selected.ImageIndex=0 then
    begin
      N:=TreeView1.Selected;
    end
    else
    begin
      if TreeView1.Selected.ImageIndex=1 then N:=TreeView1.Selected.Parent;
      if TreeView1.Selected.ImageIndex=2 then N:=TreeView1.Selected.Parent;
      if TreeView1.Selected.ImageIndex=3 then N:=TreeView1.Selected.Parent;
      if TreeView1.Selected.ImageIndex=4 then N:=TreeView1.Selected.Parent;
    end;
    if Assigned(N)=True then
    begin
      if StrToKey(StringReplace(Trim(Edit3.Text),' ','',[rfReplaceAll]),K)=True then
      begin
        N:=TreeView1.Items.AddChild(N,StringReplace(Trim(Edit3.Text),' ','',[rfReplaceAll]));
        N.ImageIndex:=1;
        N.SelectedIndex:=1;
      end
      else
      begin
        Application.MessageBox('Invalid key','Error',MB_ICONERROR + MB_OK);
      end;
    end;
  end;
end;

procedure TForm1.New1Click(Sender: TObject);
begin
  FFileName:='C:\NewKeysDB.dbkey';
  TreeView1.Items.Clear;
  Edit3.Text:='';
  Memo1.Lines.Clear;
  ValueListEditor1.Strings.Clear;
  Form1.Caption:='KeyGen v1.1 - '+FFileName;
end;

procedure TForm1.Open1Click(Sender: TObject);
var Obj: ISuperObject;
begin
  if OpenDialog1.Execute(Form1.Handle)=True then
  begin
   try
     Obj:=TSuperObject.ParseFile(Trim(OpenDialog1.FileName),False);
     FFileName:=OpenDialog1.FileName;
     TreeView1.Items.Clear;
     Edit3.Text:='';
     Memo1.Lines.Clear;
     ValueListEditor1.Strings.Clear;
     Form1.Caption:='KeyGen v1.1 - '+FFileName;
     if ObjToTree(Obj,TreeView1)=False then  Application.MessageBox('Invalid file format','Error',MB_ICONERROR + MB_OK);
   except
     on E: Exception do
     begin
       Application.MessageBox(PWideChar(E.Message),'Error',MB_ICONERROR + MB_OK);
     end;
   end;
  end;
end;

procedure TForm1.Save1Click(Sender: TObject);
begin
  if TreeView1.Items.Count>0 then
  begin
    TreeToObj(TreeView1).SaveTo(FFileName);
  end
  else
  begin
    Application.MessageBox('No data','Error',MB_ICONERROR + MB_OK);
  end;
end;

procedure TForm1.Saveas1Click(Sender: TObject);
begin
  if TreeView1.Items.Count>0 then
  begin
    if SaveDialog1.Execute(Form1.Handle)=True then
    begin
      if pos('.dbkey',SaveDialog1.FileName)>0 then
      begin
        FFileName:=Trim(SaveDialog1.FileName);
      end
      else
      begin
        FFileName:=Trim(SaveDialog1.FileName)+'.dbkey';
      end;
      TreeToObj(TreeView1).SaveTo(FFileName);
    end;
  end
  else
  begin
    Application.MessageBox('No data','Error',MB_ICONERROR + MB_OK);
  end;
end;

procedure TForm1.TreeView1Click(Sender: TObject);
var K: TKey;
    Dtn: Cardinal;
begin
  if Assigned(TreeView1.Selected)=True then
  begin
    if TreeView1.Selected.ImageIndex=0 then Edit4.Enabled:=True else  Edit4.Enabled:=False;
    case TreeView1.Selected.ImageIndex of
      1,2,3,4:
      begin
        ValueListEditor1.Strings.Clear;
        if Assigned(TreeView1.Selected.Parent)=True then
        begin
          ValueListEditor1.InsertRow('User',TreeView1.Selected.Parent.Text,True);
        end
        else
        begin
          ValueListEditor1.InsertRow('User','(Unknown)',True);
        end;
        ValueListEditor1.InsertRow('Key',TreeView1.Selected.Text,True);
        if StrToKey(TreeView1.Selected.Text,K)=True then
        begin
          if K.Flag=1 then
          begin
            ValueListEditor1.InsertRow('Type','Permanent',True);
            ValueListEditor1.InsertRow('Status','Activated',True);
          end
          else
          begin
            ValueListEditor1.InsertRow('Type','Temporary',True);
            Dtn:=DateTimeToUnix(DateOf(Now));
            if (K.Start<=Dtn) and (K.Stop>=Dtn) then
            begin
              ValueListEditor1.InsertRow('Status','Activated',True);
              ValueListEditor1.InsertRow('Current date',DateTimeToStr(DateOf(Now)),True);
              ValueListEditor1.InsertRow('Start date',DateTimeToStr(UnixToDateTime(K.Start)),True);
              ValueListEditor1.InsertRow('Stop date',DateTimeToStr(UnixToDateTime(K.Stop)),True);
              ValueListEditor1.InsertRow('Days total',IntToStr(Round(((K.Stop-K.Start)/3600)/24)),True);
              ValueListEditor1.InsertRow('Days left',IntToStr(Round(((K.Stop-Dtn)/3600)/24)),True);
            end
            else
            begin
              if Dtn>K.Stop then
              begin
                ValueListEditor1.InsertRow('Status','Expired',True);
                ValueListEditor1.InsertRow('Current date',DateTimeToStr(DateOf(Now)),True);
                ValueListEditor1.InsertRow('Start date',DateTimeToStr(UnixToDateTime(K.Start)),True);
                ValueListEditor1.InsertRow('Stop date',DateTimeToStr(UnixToDateTime(K.Stop)),True);
                ValueListEditor1.InsertRow('Days total',IntToStr(Round(((K.Stop-K.Start)/3600)/24)),True);
                ValueListEditor1.InsertRow('Days left','(Expired)',True);
              end
              else
              begin
                ValueListEditor1.InsertRow('Status','Not activated',True);
                ValueListEditor1.InsertRow('Current date',DateTimeToStr(DateOf(Now)),True);
                ValueListEditor1.InsertRow('Start date',DateTimeToStr(UnixToDateTime(K.Start)),True);
                ValueListEditor1.InsertRow('Stop date',DateTimeToStr(UnixToDateTime(K.Stop)),True);
                ValueListEditor1.InsertRow('Days total',IntToStr(Round(((K.Stop-K.Start)/3600)/24)),True);
                ValueListEditor1.InsertRow('Days left',IntToStr(Round(((K.Stop-K.Start)/3600)/24)),True);
              end;
            end;
          end;
          ValueListEditor1.InsertRow('Application id',IntToHex(K.AppId,4),True);
          ValueListEditor1.InsertRow('Install id',IntToHex(K.InsId,4),True);
        end
        else
        begin
          ValueListEditor1.InsertRow('Type','Invalid',True);
        end;
      end;
    end;
  end;
end;

procedure TForm1.User1Click(Sender: TObject);
var S: String;
    N: TTreeNode;
begin
  if Vcl.Dialogs.InputQuery('Add new user','Enter email: ',S)=True then
  begin
    if (pos('.',S)>0) and (pos('@',S)>0) then
    begin
      N:=TreeView1.Items.Add(nil,StringReplace(Trim(S),' ','',[rfReplaceAll]));
      N.ImageIndex:=0;
      N.SelectedIndex:=0;
    end
    else
    begin
      Application.MessageBox('Invalid email','Error',MB_ICONERROR + MB_OK);
    end;
  end;

end;

end.
