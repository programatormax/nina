object Form1: TForm1
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'Plugin'
  ClientHeight = 107
  ClientWidth = 646
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 8
    Top = 8
    Width = 625
    Height = 90
    Caption = 'Enter license key'
    TabOrder = 0
    object Edit1: TEdit
      Left = 96
      Top = 44
      Width = 81
      Height = 21
      CharCase = ecUpperCase
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Courier'
      Font.Style = []
      MaxLength = 8
      ParentFont = False
      TabOrder = 0
      OnKeyPress = Edit1KeyPress
    end
    object Edit2: TEdit
      Left = 200
      Top = 44
      Width = 81
      Height = 21
      CharCase = ecUpperCase
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Courier'
      Font.Style = []
      MaxLength = 8
      ParentFont = False
      TabOrder = 1
      OnKeyPress = Edit2KeyPress
    end
    object Edit3: TEdit
      Left = 312
      Top = 44
      Width = 81
      Height = 21
      CharCase = ecUpperCase
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Courier'
      Font.Style = []
      MaxLength = 8
      ParentFont = False
      TabOrder = 2
      OnKeyPress = Edit3KeyPress
    end
    object Edit4: TEdit
      Left = 424
      Top = 44
      Width = 81
      Height = 21
      CharCase = ecUpperCase
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Courier'
      Font.Style = []
      MaxLength = 8
      ParentFont = False
      TabOrder = 3
      OnKeyUp = Edit4KeyUp
    end
  end
  object Button1: TButton
    Left = 256
    Top = 24
    Width = 137
    Height = 49
    Caption = 'Color Scheme'
    TabOrder = 1
    Visible = False
    OnClick = Button1Click
  end
  object MainMenu1: TMainMenu
    Left = 40
    Top = 40
    object File1: TMenuItem
      Caption = 'File'
      object About1: TMenuItem
        Caption = 'About planswift'
        OnClick = About1Click
      end
      object Aboutplugin1: TMenuItem
        Caption = 'About plugin'
        OnClick = Aboutplugin1Click
      end
    end
  end
end
