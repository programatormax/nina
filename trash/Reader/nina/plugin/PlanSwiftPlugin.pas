{*******************************************************}
{                                                       }
{            Unit for PlanSwift application             }
{                                                       }
{*******************************************************}

unit PlanSwiftPlugin;

interface

uses

  Windows,
  SysUtils,
  DateUtils,
  IniFiles,
  Registry,
  PlanSwift9_TLB;

const

////////////////////////////////////////////////////////////////////////////////
// PLUGIN CONFIG
////////////////////////////////////////////////////////////////////////////////

  //This is plugin id
  PLUGIN_ID          = $FFFF;

  //This is plugin description
  PLUGIN_DESCRIPTION = 'PlanSwift test plugin';

////////////////////////////////////////////////////////////////////////////////
//  INTERNAL CONFIG
////////////////////////////////////////////////////////////////////////////////

  //Default key after first start
  PLUGIN_DEFAULT_KEY     = 'FFFFFFFF-FFFFFFFF-FFFFFFFF-FFFFFFFF';

  //Default storage directory for plugins
  PLUGIN_DEFAULT_DIR     = 'C:\ProgramData\planswiftplugins';

  //Default guid for plugins
  PLUGIN_DEFAULT_GUID    = '{FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF}';

  //Default install id for plugin
  PLUGIN_DEFAULT_INSTID  = $FFFF;

  //Default version for plugin
  PLUGIN_DEFAULT_VERSION = '0.0.0.0';

type

  TKeyState = (
    ksUnknown,       //Unknown key state
    ksStandby,       //Key not activated
    ksActivated,     //Key is activated
    ksExpired        //Key has expired
    );

  TKeyType = (
    ktUnknown,       //Unknown type
    ktPermanent,     //Key is permanent
    ktTemporary      //Key is temporary
    );

//Get plugin valid
function StsPluginValid: Boolean;

//Set key to plugin
function SetPluginKey(Str: AnsiString): Boolean;

//Get key from plugin
function GetPluginKey: AnsiString;

//Get plugin guid
function GetPluginGuid: AnsiString;

//Get plugin description
function GetPluginDesc: AnsiString;

//Get plugin version
function GetPluginVers: AnsiString;

//Get plugin id
function GetPluginId: Word;

//Get installation id
function GetInstallId: Word;

//Get message box about plugin
procedure AboutPlugin(hWnd: Integer);

//Check check
function GetKeyValid(Str: AnsiString): Boolean;

//Get type of key
function GetKeyType(Str: AnsiString; var Types: TKeyType): Boolean;

//Get state of key
function GetKeyState(Str: AnsiString; var State: TKeyState): Boolean;

//Get application of key
function GetKeyAppId(Str: AnsiString; var AppID: Word): Boolean;

//Get instance id of key
function GetKeyInsId(Str: AnsiString; var InsID: Word): Boolean;

//Get flag of key
function GetKeyFlag(Str: AnsiString; var Flag: Byte): Boolean;

//Get stat time of key
function GetKeyStart(Str: AnsiString; var DT: Cardinal): Boolean;

//Get stop time of key
function GetKeyStop(Str: AnsiString; var DT: Cardinal): Boolean;

//Get days total of key
function GetKeyDaysTotal(Str: AnsiString; var C: Integer): Boolean;

//Get days left of key
function GetKeyDaysLeft(Str: AnsiString; var C: Integer): Boolean;

var

  PlanSwift: IPlanSwift;

implementation

type

  TKey = packed record
    Prefx: Byte;
    RrdId: Byte;
    Crc: Byte;
    Flag: Byte;
    AppId: Word;
    InsId: Word;
    Start: Cardinal;
    Stop: Cardinal;
  end;

var

 IniFile: TIniFile;

function GetPluginId: Word;
begin
  Result:=PLUGIN_ID;
end;

function GetInstallId: Word;
begin
  Result:=IniFile.ReadInteger('plugin','ins',PLUGIN_DEFAULT_INSTID);
end;

function CRC8(data: pbyte; len: byte): byte;
var n, crc, extract,tempI,sum : byte;
begin
  crc := 0;
  for n := len downto 1 do
  begin
    extract := data^;
    inc (data);
    for tempI := 8 downto 1 do begin
      sum := (crc xor extract) and 1;
      crc := crc shr 1;
      if sum = 1 then crc := crc xor $8C;
      extract := extract shr 1;
    end;
  end;
  exit(crc);
end;

function StrToKey(Str: AnsiString; var Key: TKey): Boolean;
var Buf: array [0..15] of Byte;
    L: Cardinal;
    Q: Cardinal;
    c: Byte;
    K: AnsiString;
    r: Byte;
    n: Integer;
begin
  Result:=False;
  K:=Trim(Str);
  if (Length(K)=35) and
     (K[9]='-') and
     (K[18]='-') and
     (K[27]='-') then
  begin
    K:=StringReplace(K,'-','',[rfReplaceAll,rfIgnoreCase]);
    L:=1;
    Q:=0;
    while L<32 do
    begin
      Buf[Q]:=StrToInt('$'+K[L]+K[L+1]);
      L:=L+2;
      Q:=Q+1;
    end;
    r:=Buf[1];
    for n:=Low(Buf) to High(Buf) do
    begin
      Buf[n]:=Buf[n] xor Byte(r+n*n);
    end;
    Buf[1]:=r;
    c:=Buf[2];
    Buf[2]:=0;
    if CRC8(@Buf[0],16)=C then
    begin
      if Buf[0]=$0A then
      begin
        Key.Prefx:=Buf[0];
        Key.RrdId:=Buf[1];
        Key.Crc:=Buf[2];
        Key.Flag:=Buf[3];
        Key.AppId:=(Buf[4]+(Buf[5]*256));
        Key.InsId:=(Buf[6]+(Buf[7]*256));
        Move(Buf[8],Key.Start,4);
        Move(Buf[12],Key.Stop,4);
        if (Key.AppId=$FFFF) and (Key.InsId=$FFFF) then
        begin
          Result:=True;
        end
        else
        begin
          if (Key.AppId=$FFFF) and (Key.InsId<>$FFFF) then
          begin
            if Key.InsId=GetInstallId then Result:=True;
          end
          else
          begin
            if (Key.AppId<>$FFFF) and (Key.InsId=$FFFF) then
            begin
              if Key.AppId=GetPluginId then Result:=True;
            end
            else
            begin
              if (Key.AppId<>$FFFF) and (Key.InsId<>$FFFF) then
              begin
                if (Key.AppId=GetPluginId) and (Key.InsId=GetInstallId) then Result:=True;
              end
            end;
          end;
        end;
      end;
    end;
  end;
end;

function GetKeyValid(Str: AnsiString): Boolean;
var Key: TKey;
begin
  Result:=StrToKey(Str,Key);
end;

function GetKeyType(Str: AnsiString; var Types: TKeyType): Boolean;
var
  Key: TKey;
  Dtn: Cardinal;
begin
  Result:=False;
  if StrToKey(Str,Key)=True then
  begin
    case Key.Flag of
      0: Types:=ktTemporary;
      1: Types:=ktPermanent;
    else
      Types:=ktUnknown;
    end;
    Result:=True;
  end;
end;

function GetKeyState(Str: AnsiString; var State: TKeyState): Boolean;
var
  Key: TKey;
  Dtn: Cardinal;
begin
  Result:=False;
  if StrToKey(Str,Key)=True then
  begin
    if Key.Flag=1 then
    begin
      State:=ksActivated;
    end
    else
    begin
      Dtn:=DateTimeToUnix(DateOf(Now));
      if (Key.Start<=Dtn) and (Key.Stop>=Dtn) then
      begin
        State:=ksActivated;
      end
      else
      begin
        if Dtn>Key.Stop then
        begin
          State:=ksExpired;
        end
        else
        begin
          if Dtn<Key.Start then
          begin
            State:=ksStandby;
          end
          else
          begin
            State:=ksUnknown;
          end;
        end;
      end;
    end;
    Result:=True;
  end;
end;

function GetKeyAppId(Str: AnsiString; var AppID: Word): Boolean;
var Key: TKey;
begin
  Result:=False;
  if StrToKey(Str,Key)=True then
  begin
    AppID:=Key.AppId;
    Result:=True;
  end;
end;

function GetKeyInsId(Str: AnsiString; var InsID: Word): Boolean;
var Key: TKey;
begin
  Result:=False;
  if StrToKey(Str,Key)=True then
  begin
    InsID:=Key.InsId;
    Result:=True;
  end;
end;

function GetKeyFlag(Str: AnsiString; var Flag: Byte): Boolean;
var Key: TKey;
begin
  Result:=False;
  if StrToKey(Str,Key)=True then
  begin
    Flag:=Key.Flag;
    Result:=True;
  end;
end;

function GetKeyStart(Str: AnsiString; var DT: Cardinal): Boolean;
var Key: TKey;
begin
  Result:=False;
  if StrToKey(Str,Key)=True then
  begin
    DT:=Key.Start;
    Result:=True;
  end;
end;

function GetKeyStop(Str: AnsiString; var DT: Cardinal): Boolean;
var Key: TKey;
begin
  Result:=False;
  if StrToKey(Str,Key)=True then
  begin
    DT:=Key.Stop;
    Result:=True;
  end;
end;

function GetKeyDaysTotal(Str: AnsiString; var C: Integer): Boolean;
var
  Key: TKey;
  Dtn: Cardinal;
begin
  Result:=False;
  if StrToKey(Str,Key)=True then
  begin
    if Key.Flag=1 then
    begin
      C:=0;
    end
    else
    begin
      Dtn:=DateTimeToUnix(DateOf(Now));
      if (Key.Start<=Dtn) and (Key.Stop>=Dtn) then
      begin
        C:=Round(((Key.Stop-Key.Start)/3600)/24);
      end
      else
      begin
        if Dtn>Key.Stop then
        begin
          C:=-1;
        end
        else
        begin
          if Dtn<Key.Start then
          begin
            C:=Round(((Key.Stop-Key.Start)/3600)/24);
          end
          else
          begin
            C:=-1;
          end;
        end;
      end;
    end;
    Result:=True;
  end;
end;

function GetKeyDaysLeft(Str: AnsiString; var C: Integer): Boolean;
var
  Key: TKey;
  Dtn: Cardinal;
begin
  Result:=False;
  if StrToKey(Str,Key)=True then
  begin
    if Key.Flag=1 then
    begin
      C:=0;
    end
    else
    begin
      Dtn:=DateTimeToUnix(DateOf(Now));
      if (Key.Start<=Dtn) and (Key.Stop>=Dtn) then
      begin
        C:=Round(((Key.Stop-Dtn)/3600)/24);
      end
      else
      begin
        if Dtn>Key.Stop then
        begin
          C:=-1;
        end
        else
        begin
          if Dtn<Key.Start then
          begin
            C:=Round(((Key.Stop-Key.Start)/3600)/24);
          end
          else
          begin
            C:=-1;
          end;
        end;
      end;
    end;
    Result:=True;
  end;
end;

function StsPluginValid: Boolean;
var State: TKeyState;
begin
  if (GetKeyState(GetPluginKey,State)=True) and (State=ksActivated) then Result:=True else Result:=False;
end;

function SetPluginKey(Str: AnsiString): Boolean;
var State: TKeyState;
begin
  if GetKeyState(Str,State)=True then
  begin
    IniFile.WriteString('plugin','key',Str);
    if State=ksActivated then Result:=True else Result:=False;
  end
  else
  begin
    Result:=False;
  end;
end;

function GetPluginKey: AnsiString;
begin
  Result:=IniFile.ReadString('plugin','key',PLUGIN_DEFAULT_KEY);
end;

function GetPluginGuid: AnsiString;
begin
  if Assigned(PlanSwift) then Result:=PlanSwift.GetPropertyResultAsString('\','GUID','') else Result:=PLUGIN_DEFAULT_GUID;
end;

function GetPluginDesc: AnsiString;
begin
  Result:=PLUGIN_DESCRIPTION;
end;

function GetPluginVers: AnsiString;
var infoSize: DWORD;
    verBuf:   pointer;
    verSize:  UINT;
    wnd:      UINT;
    FixedFileInfo : PVSFixedFileInfo;
    sgFileName: string;
begin
  Result:=PLUGIN_DEFAULT_VERSION;
  sgFileName:=ParamStr(0);
  if FileExists(sgFileName) then
  begin
    infoSize:=GetFileVersioninfoSize(PChar(sgFileName), wnd);
    if infoSize <> 0 then
    begin
      GetMem(verBuf, infoSize);
      try
        if GetFileVersionInfo(PChar(sgFileName), wnd, infoSize, verBuf) then
        begin
          VerQueryValue(verBuf, '\', Pointer(FixedFileInfo), verSize);
          Result := IntToStr(FixedFileInfo.dwFileVersionMS div $10000) + '.' +
                    IntToStr(FixedFileInfo.dwFileVersionMS and $0FFFF) + '.' +
                    IntToStr(FixedFileInfo.dwFileVersionLS div $10000) + '.' +
                    IntToStr(FixedFileInfo.dwFileVersionLS and $0FFFF);
        end;
      finally
        FreeMem(verBuf);
      end;
    end;
  end;
end;

procedure AboutPlugin(hWnd: Integer);
var Str: AnsiString;
    Sts: TKeyState;
    Typ: TKeyType;
    C: Integer;
begin
  Str:='';
  Str:=Str+'Description: '+GetPluginDesc+#13#10;
  Str:=Str+'Version: '+GetPluginVers+#13#10;
  Str:=Str+'PluginId: '+IntToHex(GetPluginId,4)+#13#10;
  Str:=Str+'InstallId: '+IntToHex(GetInstallId,4)+#13#10;
  Str:=Str+'Key: '+GetPluginKey+#13#10;
  if GetKeyState(GetPluginKey,Sts)=True then
  begin
    case Sts of
      ksUnknown:
      begin
        Str:=Str+'License: Unknown'+#13#10;
      end;
      ksStandby:
      begin
        Str:=Str+'License: Standby'+#13#10;
      end;
      ksActivated:
      begin
        Str:=Str+'License: Activated'+#13#10;
      end;
      ksExpired:
      begin
        Str:=Str+'License: Expired'+#13#10;
      end;
    else
      Str:=Str+'License: Invalid'+#13#10;
    end;
  end
  else
  begin
    Str:=Str+'License: Invalid'+#13#10;
  end;
  if GetKeyType(GetPluginKey,Typ)=True then
  begin
    case Typ of
      ktUnknown:
      begin
        Str:=Str+'Type: Unknown'+#13#10;
      end;
      ktPermanent:
      begin
        Str:=Str+'Type: Permanent'+#13#10;
      end;
      ktTemporary:
      begin
        Str:=Str+'Type: Temporary'+#13#10;
      end;
    else
      Str:=Str+'Type: Invalid'+#13#10;
    end;
  end
  else
  begin
    Str:=Str+'Type: Invalid'+#13#10;
  end;
  if GetKeyDaysLeft(GetPluginKey,C)=True then
  begin
    Str:=Str+'Days left: '+IntToStr(C)+#13#10;
  end
  else
  begin
    Str:=Str+'Days left: Invalid'+#13#10;
  end;
  MessageBoxA(0,PAnsiChar(Str),'About plugin',MB_ICONINFORMATION+MB_OK);
end;

initialization

  randomize;
  PlanSwift:=CoPlanSwift.Create;
  ForceDirectories(PLUGIN_DEFAULT_DIR);
  IniFile:=TIniFile.Create(PLUGIN_DEFAULT_DIR+'\'+IntToHex(GetPluginId,4)+'.ini');
  if IniFile.ValueExists('plugin','key')=False then IniFile.WriteString('plugin','key',PLUGIN_DEFAULT_KEY);
  if IniFile.ValueExists('plugin','ins')=False then IniFile.WriteInteger('plugin','ins',Random(65535));

finalization

  IniFile.Destroy;
  IniFile:=nil;

end.
