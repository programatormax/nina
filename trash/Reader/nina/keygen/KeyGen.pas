unit KeyGen;

interface

uses

  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ComCtrls, Vcl.StdCtrls, DateUtils;

type

  TKey = packed record
    Prefx: Byte;     //Auto - Prefix for identify
    RrdId: Byte;     //Auto - Random xor index
    Crc: Byte;       //Auto - Check sum
    Flag: Byte;      //Must be filled - Key's flags
    AppId: Word;     //Must be filled - Application id
    InsId: Word;     //Must be filled - Application install id
    Start: Cardinal; //Must be filled - Start range time
    Stop: Cardinal;  //Must be filled - Stop range time
  end;

  TForm1 = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    DateTimePicker1: TDateTimePicker;
    Label2: TLabel;
    DateTimePicker2: TDateTimePicker;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    Label3: TLabel;
    Edit1: TEdit;
    Label4: TLabel;
    Edit2: TEdit;
    Edit3: TEdit;
    Label5: TLabel;
    Button1: TButton;
    Button2: TButton;
    Memo1: TMemo;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var

  Form1: TForm1;

implementation

{$R *.dfm}

//------------------------------------------------------------------------------
// Key format
//------------------------------------------------------------------------------

//byte CRC8(const byte *data, byte len) {
//  byte crc = 0x00;
//  while (len--) {
//    byte extract = *data++;
//    for (byte tempI = 8; tempI; tempI--) {
//      byte sum = (crc ^ extract) & 0x01;
//      crc >>= 1;
//      if (sum) {
//        crc ^= 0x8C;
//      }
//      extract >>= 1;
//    }
//  }
//  return crc;
//}

//Check sum
function CRC8(data: pbyte; len: byte): byte;
var n, crc, extract,tempI,sum : byte;
begin
  crc := 0;
  for n := len downto 1 do
  begin
    extract := data^;
    inc (data);
    for tempI := 8 downto 1 do
    begin
      sum := (crc xor extract) and 1;
      crc := crc shr 1;
      if sum = 1 then crc := crc xor $8C;
      extract := extract shr 1;
    end;
  end;
  exit(crc);
end;

//Convert Key To String
function KeyToStr(var Key: TKey): AnsiString;
var Buf: array [0..15] of Byte;
      r: Byte;
     n: Integer;
begin
  Result:='';
  r:=Key.RrdId;
  Buf[0]:=Key.Prefx;
  Buf[1]:=Key.RrdId;
  Buf[2]:=0;
  Buf[3]:=Key.Flag;
  Move(Key.AppId,Buf[4],2);
  Move(Key.InsId,Buf[6],2);
  Move(Key.Start,Buf[8],4);
  Move(Key.Stop,Buf[12],4);
  Buf[2]:=CRC8(@Buf[0],16);
  for n:=Low(Buf) to High(Buf) do
  begin
    Buf[n]:=Buf[n] xor Byte(r+n*n);
  end;
  Buf[1]:=r;
  for n:=Low(Buf) to High(Buf) do
  begin
    Result:=Result+IntToHex(Buf[n],2);
  end;
  Result:=Copy(Result,1,8)+'-'+Copy(Result,9,8)+'-'+Copy(Result,17,8)+'-'+Copy(Result,25,8);
end;

//Convert Str To Key
function StrToKey(Str: AnsiString; var Key: TKey): Boolean;
var Buf: array [0..15] of Byte;
    L: Cardinal;
    Q: Cardinal;
    c: Byte;
    K: AnsiString;
    r: Byte;
    n: Integer;
begin
  Result:=False;
  K:=Trim(Str);
  if (Length(K)=35) and
     (K[9]='-') and
     (K[18]='-') and
     (K[27]='-') then
  begin
    K:=StringReplace(K,'-','',[rfReplaceAll,rfIgnoreCase]);
    L:=1;
    Q:=0;
    while L<32 do
    begin
      Buf[Q]:=StrToInt('$'+K[L]+K[L+1]);
      L:=L+2;
      Q:=Q+1;
    end;
    r:=Buf[1];
    for n:=Low(Buf) to High(Buf) do
    begin
      Buf[n]:=Buf[n] xor Byte(r+n*n);
    end;
    Buf[1]:=r;
    c:=Buf[2];
    Buf[2]:=0;
    if CRC8(@Buf[0],16)=C then
    begin
      if Buf[0]=$0A then
      begin
        Key.Prefx:=Buf[0];
        Key.RrdId:=Buf[1];
        Key.Crc:=Buf[2];
        Key.Flag:=Buf[3];
        Key.AppId:=(Buf[4]+(Buf[5]*256));
        Key.InsId:=(Buf[6]+(Buf[7]*256));
        Move(Buf[8],Key.Start,4);
        Move(Buf[12],Key.Stop,4);
        Result:=True;
      end;
    end;
  end;
end;

//Button Generate
procedure TForm1.Button1Click(Sender: TObject);
var Key: TKey;
begin
  if Edit1.Text='' then raise Exception.Create('Application id parameter must be filled');
  if Length(Trim(Edit1.Text))<>4 then raise Exception.Create('Application id parameter length must be 4 not '+IntToStr(Length(Trim(Edit1.Text))));
  if Edit2.Text='' then raise Exception.Create('Install id parameter must be filled');
  if Length(Trim(Edit2.Text))<>4 then raise Exception.Create('Install id parameter length must be 4 not '+IntToStr(Length(Trim(Edit2.Text))));
  if (RadioButton1.Checked=False) and (RadioButton2.Checked=False) then raise Exception.Create('Permanent or Temporary types not defined');
  if DateTimeToUnix(DateTimePicker1.DateTime)>DateTimeToUnix(DateTimePicker2.DateTime) then raise Exception.Create('Start date must be less then stop date');
  randomize;
  Key.Prefx:=$0A;
  Key.RrdId:=Random(255);
  Key.AppId:=StrToInt('$'+Trim(Edit1.Text));
  Key.InsId:=StrToInt('$'+Trim(Edit2.Text));
  if RadioButton2.Checked=True then
  begin
    Key.Flag:=$01;
    Key.Start:=Random(MaxDword);
    Key.Stop:=Random(MaxDword);
  end
  else
  begin
    Key.Flag:=$00;
    Key.Start:=DateTimeToUnix(DateTimePicker1.Date);
    Key.Stop:=DateTimeToUnix(DateTimePicker2.Date);
  end;
  Edit3.Text:=KeyToStr(Key);
  Button2Click(nil);
end;

//Button Check
procedure TForm1.Button2Click(Sender: TObject);
var Key: TKey;
    Str: AnsiString;
    Dtn: Cardinal;
begin
  if StrToKey(Edit3.Text,Key)=True then
  begin
    Str:='Key: '+Trim(Edit3.Text)+#13#10;
    Str:=Str+'Application id: '+IntToHex(Key.AppId,4)+#13#10;
    Str:=Str+'Install id: '+IntToHex(Key.InsId,4)+#13#10;
    if Key.Flag=1 then
    begin
      Str:=Str+'Type: PERMANENT'+#13#10;
      Str:=Str+'Status: [ ACTIVATED ]'+#13#10;
      Edit3.Color:=clGreen;
    end
    else
    begin
      Str:=Str+'Type: TEMPORARY'+#13#10;
      Dtn:=DateTimeToUnix(DateOf(Now));
      if (Key.Start<=Dtn) and (Key.Stop>=Dtn) then
      begin
        Str:=Str+'Status: [ ACTIVATED ]'+#13#10;
        Edit3.Color:=clGreen;
      end
      else
      begin
        if Dtn>Key.Stop then
        begin
          Str:=Str+'Status: [ EXPIRED ]'+#13#10;
        end
        else
        begin
          Str:=Str+'Status: [ NOT ACTIVATED ]'+#13#10;
        end;
        Edit3.Color:=clYellow;
      end;
      Str:=Str+'Current date: '+DateTimeToStr(DateOf(Now))+#13#10;
      Str:=Str+'Start date: '+DateTimeToStr(UnixToDateTime(Key.Start))+#13#10;
      Str:=Str+'Stop date: '+DateTimeToStr(UnixToDateTime(Key.Stop))+#13#10;
      Str:=Str+'Days total: '+IntToStr(Round(((Key.Stop-Key.Start)/3600)/24));
    end;
  end
  else
  begin
    Str:='Key: '+Trim(Edit3.Text)+#13#10;
    Str:=Str+'Type: INVALID';
    Edit3.Color:=clRed;
  end;
  Memo1.Lines.Clear;
  Memo1.Lines.Text:=Str;
end;

//On Show Window
procedure TForm1.FormShow(Sender: TObject);
var Dtm: TDateTime;
begin
  Dtm:=Now;
  DateTimePicker1.Date:=DateOf(Dtm);
  DateTimePicker2.Date:=DateOf(Dtm);
end;

end.
