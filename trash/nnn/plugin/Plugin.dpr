program Plugin;

uses
  Vcl.Forms,
  ExternalPlugin in 'ExternalPlugin.pas' {Form1},
  PlanSwiftPlugin in 'PlanSwiftPlugin.pas';

{$R *.res}

var

  Sts: TKeyState;

begin
  if PlanSwiftPlugin.GetKeyState(GetPluginKey,Sts)=True then
  begin
    case Sts of
      ksUnknown:
      begin
        Application.Initialize;
        Application.MainFormOnTaskbar:=True;
        Application.CreateForm(TForm1, Form1);
        Application.Run;
      end;
      ksStandby:
      begin
        //Nothing to do
      end;
      ksActivated:
      begin
        //Nothing to do
      end;
      ksExpired:
      begin
        PlanSwiftPlugin.PluginUninstall;
        PlanSwiftPlugin.AboutPlugin(0);
      end
    else
      PlanSwiftPlugin.AboutPlugin(0);
    end;
  end
  else
  begin
    PlanSwiftPlugin.PluginUninstall;
    Application.Initialize;
    Application.MainFormOnTaskbar := True;
    Application.CreateForm(TForm1, Form1);
    Application.Run;
  end;
end.
