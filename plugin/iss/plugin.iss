;PlanSwift install script

#define plugin_name        "Rocket plugin";
#define plugin_version     "1.0.0.0";
#define plugin_company     "PlanSwift";
#define plugin_copyright   "All rights reserved 2022";
#define plugin_description "Planswift plugin";
#define plugin_buildir     GetDateTimeString('dd/mm/yyyy', '-', ':')  

[Setup]

AppName={#plugin_name}
AppVerName={#plugin_name} {#plugin_version}
DefaultDirName={code:GetPlanSwiftDataDir}
DefaultGroupName=PlanSwift10
AllowNoIcons=false
OutputDir=..\..\build\plugin\{#plugin_buildir}
OutputBaseFilename=RocketPlanSwiftPlugin.{#plugin_version}
Compression=lzma
SolidCompression=true
WizardImageFile=.\wizard.bmp
AppID={code:GetPluginGuid}
ShowLanguageDialog=no
WizardImageStretch=false
DisableProgramGroupPage=true
VersionInfoTextVersion={#plugin_version}
VersionInfoVersion={#plugin_version}
VersionInfoCompany={#plugin_company}
VersionInfoCopyright={#plugin_copyright}
VersionInfoDescription={#plugin_description}
DisableWelcomePage=no
UserInfoPage=yes
UsePreviousLanguage=no
AlwaysShowDirOnReadyPage=yes
DisableDirPage=yes

;[Tasks]
;Name: desktopicon; Description: {cm:CreateDesktopIcon}; GroupDescription: {cm:AdditionalIcons}; Flags: unchecked

[Files]
          
Source: "..\pck\HD Assemblies\*"; DestDir: "{app}\Storages\Local\Templates\HD Assemblies"; Flags: ignoreversion recursesubdirs;
Source: "..\pck\Informer\*"; DestDir: "{app}\Plugins\Informer"; Flags: ignoreversion recursesubdirs;
Source: "..\pck\SKU Data\*"; DestDir: "{app}\Lists\SKU Data"; Flags: ignoreversion recursesubdirs;
Source: "..\pck\SKU Database\SKU Data.mdb"; DestDir: "{sd}\ProgramData\planswiftplugin\"; Flags: ignoreversion;                                  
Source: "..\pck\Checker\Checker.exe"; DestDir: {code:GetPlanSwiftMainDir}\; Flags: ignoreversion;
                     
[Run]

Filename: "{code:GetPlanSwiftMainDir}\checker.exe"; Parameters: "/I {code:GetPluginGuid}"; Description: "Check license"; Flags: postinstall nowait skipifsilent;
Filename: "{code:GetPlanSwiftMainDir}\checker.exe"; Parameters: "/P {code:GetPluginGuid}"; Flags: nowait;                           
Filename: "{code:GetPlanSwiftMainDir}\checker.exe"; Parameters: "/P {code:GetPluginGuid}"; Flags: nowait;

                                           
[Registry]
                                                              
;Plugin
Root: HKCU; SubKey: Software\PlanSwift\Plugins\{code:GetPluginGuid}; ValueType: string; ValueName: Guid; ValueData: {code:GetPluginGuid}; Flags: uninsdeletekey;
Root: HKCU; SubKey: Software\PlanSwift\Plugins\{code:GetPluginGuid}; ValueType: string; ValueName: Appid; ValueData: {code:GetPluginAppid}; Flags: uninsdeletekey;
Root: HKCU; SubKey: Software\PlanSwift\Plugins\{code:GetPluginGuid}; ValueType: string; ValueName: Insid; ValueData: {code:GetPluginInsId}; Flags: uninsdeletekey;
Root: HKCU; SubKey: Software\PlanSwift\Plugins\{code:GetPluginGuid}; ValueType: string; ValueName: Name; ValueData: {code:GetPluginName}; Flags: uninsdeletekey;
Root: HKCU; SubKey: Software\PlanSwift\Plugins\{code:GetPluginGuid}; ValueType: string; ValueName: Version; ValueData: {code:GetPluginVersion}; Flags: uninsdeletekey;
Root: HKCU; SubKey: Software\PlanSwift\Plugins\{code:GetPluginGuid}; ValueType: string; ValueName: Description; ValueData: {code:GetPluginDescription}; Flags: uninsdeletekey;
Root: HKCU; SubKey: Software\PlanSwift\Plugins\{code:GetPluginGuid}; ValueType: string; ValueName: UserName; ValueData: {userinfoname}; Flags: uninsdeletekey
Root: HKCU; SubKey: Software\PlanSwift\Plugins\{code:GetPluginGuid}; ValueType: string; ValueName: Organization; ValueData: {userinfoorg}; Flags: uninsdeletekey
Root: HKCU; SubKey: Software\PlanSwift\Plugins\{code:GetPluginGuid}; ValueType: string; ValueName: License; ValueData: {userinfoserial}; Flags: uninsdeletekey
Root: HKCU; SubKey: Software\PlanSwift\Plugins\{code:GetPluginGuid}; ValueType: string; ValueName: 0; ValueData: {app}\Storages\Local\Templates\HD Assemblies; Flags: uninsdeletekey;
Root: HKCU; SubKey: Software\PlanSwift\Plugins\{code:GetPluginGuid}; ValueType: string; ValueName: 1; ValueData: {app}\Lists\SKU Data; Flags: uninsdeletekey;
Root: HKCU; SubKey: Software\PlanSwift\Plugins\{code:GetPluginGuid}; ValueType: string; ValueName: 2; ValueData: {sd}\ProgramData\planswiftplugin\SKU Data.mdb; Flags: uninsdeletekey;
Root: HKCU; Subkey: Software\Microsoft\Windows\CurrentVersion\Run; ValueType: string; ValueName: {code:GetPluginGuid}; ValueData: "{code:GetPlanSwiftMainDir}\checker.exe /P ""{code:GetPluginGuid}"""; Flags: uninsdeletevalue;
                                                                     
[Code]   
                                                                
const                                                       

  PLUGIN_GUID        = '{BDE9810E-2AA0-461B-B5AE-D5FDD1CA8431}';
  PLUGIN_APPID       = $2AA0;
  PLUGIN_NAME        = 'Rocket plugin';
  PLUGIN_VERSION     = '1.0.0.1';
  PLUGIN_COMPANY     = 'You name company here';
  PLUGIN_DESCRIPTION = 'Rocket PlanSwift plugin';

//function Time(ATimerPtr: integer): integer; external '_time32@msvcrt.dll cdecl';


function IsPlanSwiftInstall: Boolean;
var R: String;
begin
  Result:=RegQueryStringValue(HKLM,'SOFTWARE\WOW6432Node\PlanSwift10','InstallDir',R);
end;

function InitializeSetup(): Boolean;
begin
  if IsPlanSwiftInstall=True then
  begin
    Result:=True;
  end
  else
  begin
    MsgBox('The PlanSwift application is not installed, please install application and try again', mbInformation, MB_OK);
    Result:=False;
  end;
end;

function GetPlanSwiftMainDir(Param: String): String;
begin
  if RegQueryStringValue(HKLM,'SOFTWARE\WOW6432Node\PlanSwift10','InstallDir',Result)=False then Result:='C:\Program Files (x86)\PlanSwift10';
end;

function GetPlanSwiftDataDir(Param: String): String;
begin
  if RegQueryStringValue(HKLM,'SOFTWARE\WOW6432Node\PlanSwift10','LocalDataPath',Result)=False then  Result:='C:\Program Files (x86)\PlanSwift10\Data';
end;

function GetPluginGuid(Param: String): String;
begin
  Result:=PLUGIN_GUID;
end;

function GetPluginAppId(Param: String): String;
begin
  Result:=IntToStr(PLUGIN_APPID);
end; 

function GetPluginInsId(Param: String): String;
begin
  Result:=IntToStr(random(65535));
end;

function GetPluginName(Param: String): String;
begin
  Result:=PLUGIN_NAME;
end;

function GetPluginCompany(Param: String): String;
begin
  Result:=PLUGIN_COMPANY;
end;

function GetPluginVersion(Param: String): String;
begin
  Result:=PLUGIN_VERSION;
end;

function GetPluginDescription(Param: String): String;
begin
  Result:=PLUGIN_DESCRIPTION;
end;

function CheckSerial(Serial: String): Boolean;
begin
  if (Length(Serial)=35) and
     (Copy(Serial,9,1)='-') and
     (Copy(Serial,18,1)='-') and
     (Copy(Serial,27,1)='-') and
     (StrToInt('$'+Copy(Serial,1,2)) xor StrToInt('$'+Copy(Serial,3,2))=$0A) then
  begin
    Result:=True;
  end
  else
  begin
    Result:=False;
  end;
end;
